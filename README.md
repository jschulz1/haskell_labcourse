# Haskell lecture 

A lecture to teach Haskell in a 2 weeks block lab course.

## lecture notes

* [01 introduction and motivation](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/01%20introduction%20and%20motivation.ipynb)
* [02 first steps](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/02%20first%20steps.ipynb)
* [03 lists and tuples](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/03%20lists%20and%20tuples.ipynb)
* [04 types](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/04%20types.ipynb)
* [05 pattern matching](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/05%20pattern%20matching.ipynb)
* [06 syntactic sugar](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/06%20syntactic%20sugar.ipynb)
* [07 higher order functions](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/07%20higher%20order%20functions.ipynb)
* [08 higher order tools](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/08%20higher%20order%20tools.ipynb)
* [09 reductions](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/09%20reductions.ipynb)
* [10 recursion](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/10%20recursion.ipynb)
* [11 tail recursion](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/11%20tail%20recursion.ipynb)
* [12 modules, compilation and tracing](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/12%20modules,%20compilation%20and%20tracing.ipynb)
* [13 type classes](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/13%20type%20classes.ipynb)
* [14 monads](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/14%20monads.ipynb)
* [15 io](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/15%20io.ipynb)
* [16 monads II](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/16%20monads%20II.ipynb)
* [17 lenses](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/17%20lenses.ipynb)
* [18 charts](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/18%20charts.ipynb)
* [19 parallel arrays](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/19%20parallel%20arrays.ipynb)
* [20 linear algebra](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/20%20linear%20algebra.ipynb)
* [21 integration and differentation](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/21%20integration%20and%20differentation.ipynb)
* [22 boundary value problem](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/22%20boundary%20value%20problem.ipynb)
* [23 ODE](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/23%20ODE.ipynb)
* [24 runge-kutta methods](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/24%20runge-kutta%20methods.ipynb)
* [25 poisson equation (PDE)](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/25%20poisson%20equation%20(PDE).ipynb)
* [26 continous optimization](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/26%20continous%20optimization.ipynb)
* [27 inverse](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/27%20inverse.ipynb)
* [28 minsurf](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/28%20minsurf.ipynb)
* [Untitled](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/Untitled.ipynb)
* [shortest path](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/haskell_labcourse/raw/master/lecture/shortest%20path.ipynb)

## viewing of the notebooks

you can view the notebooks with an actual ihaskell kernel for jupyter (ipython 3.x). 
It is also possible to view them with the nbviewer from ipython:

viewed in nbviewer directly from the repository

# Installation and use

## Requirements

at minimum

 - ghc 7.10+
 - charts
 - ihaskell
 - hmatrix

## How to use IHaskell at home via SSH tunnel

### At the NAM

Create a file ~/.ipython/profile_default/ipython_notebook_config.py containing

    c = get_config()
    c.NotebookApp.port = <port>

where `<port>` should be replaced by a randomly chosen port number between 50000 and 60000. As it is possible that multiple users want to run notebook servers on a single computer, it is important to choose a unique port in order to avoid conflicts.

### On your home computer

Open a terminal and run

    ssh -L <port>:localhost:<port> <user>@<host>.num.math.uni-goettingen.de ihaskell-notebook

where `<port>` is the same port as chosen above, `<user>` is your user name and `<host>` is one of `c3`, `c4`, etc, up to `c7`. On Windows, use an ssh client like PuTTY and connect to one of the hosts with enabled forwarding of `<port>`.

You should now be able to direct your browser to `localhost:<port>` to access the notebook interface.

## How to use IHaskell at home via X2go

get and install x2goclient from http://wiki.x2go.org. 

Create a session entry with 

    - host: login.num.math.uni-goettingen.de or one of the compute-servers c3-c7.num.math.uni-goettingen.de 
    - session type: XFCE
    - login: your student username

then you can connect with your usual credentials and get a remote desktop (like sitting directly in front of the computers)

# Converting notebooks 
## converting md-files to ipython (and back)

you can use the `md2ipynb` script or notedown (https://github.com/aaren/notedown) .

## converting lhs to python (and back)
 
    ihaskell convert -i newton.lhs -o newton.ipynb 

# visualization with python

because haskell misses a good 3D visualization library we use python for it. There is a script which takes X, Y and Z matrices to make a surface or contour plot.

```
./vis.py 
usage: vis.py [-h] --xmat XMAT --ymat YMAT --zmat ZMAT [--triangles TRIANGLES]
              [--type {surf,wireframe,contour,surf_contour,mayavi_surf,triangles}]
```

