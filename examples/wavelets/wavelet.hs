{-# OPTIONS_GHC -O3 #-}
{-# LANGUAGE FlexibleContexts #-}

import Data.Array.Repa as Repa hiding ((++))
import Codec.Picture.Repa (readImageRGB, imgData)
import Data.Array.Repa.Eval (Load(..))

type Img r = Array r DIM2 Double

loadImage :: IO (Img U)
loadImage = do
     result <- readImageRGB "grumpy.png"
     let imageRGB = case result of
                     Left err  -> error err
                     Right img -> img
     return . sumS . Repa.map fromIntegral . imgData $ imageRGB

compS :: Load r DIM2 Double => Img r -> Img U
compS = computeS

filter1d :: Source r Double => (Double -> Double -> Double) -> Img r -> Img D
filter1d op img = traverse img resize modify
    where resize (Z:.i:.j) = Z:.i:.(j `div` 2)
          modify get (Z:.i:.j) = get (Z:.i:.(2*j)) `op` get (Z:.i:.(2*j+1))

step1d :: Source r Double => Img r -> Img D
step1d img = append (filter1d lowpass img) (filter1d highpass img)
    where lowpass x y = (x + y) / 2
          highpass x y = (x - y) / 2

waveletStep :: Source r Double => Img r -> Img U
waveletStep = compS . transpose . step1d . transpose . step1d

insertTopLeft :: (Source r1 Double, Source r2 Double) => Img r1 -> Img r2 -> Img D
insertTopLeft img topleft = traverse img id modify
    where sh = extent topleft
          modify get idx
              | inShape sh idx = topleft ! idx
              | otherwise      = get idx

extractTopLeft :: Source r Double => Img r -> Img D
extractTopLeft img = extract (Z:.0:.0) (Z:.(sx `div` 2):.(sy `div` 2)) img
    where Z:.sx:.sy = extent img

waveletTransform :: Load r DIM2 Double => Int -> Img r -> Img U
waveletTransform 0 img = compS img
waveletTransform n img = compS $ insertTopLeft img' topleft
    where img' = waveletStep img
          topleft = waveletTransform (n-1) $ extractTopLeft img'

inverseFilter1d :: Source r Double => (Double -> Double -> Double) -> Img r -> Img D
inverseFilter1d op img = Repa.zipWith op left right
    where Z:.sx:.sy = extent img
          half      = Z:.sx:.(sy `div` 2)
          left      = extract (Z:.0:.0) half img
          right     = extract (Z:.0:.(sy `div` 2)) half img

inverseStep1d :: Source r Double => Img r -> Img D
inverseStep1d img = interleave2 (inverseFilter1d (+) img) (inverseFilter1d (-) img)

inverseWaveletStep :: Source r Double => Img r -> Img U
inverseWaveletStep = compS . inverseStep1d . transpose . compS . inverseStep1d . transpose

inverseWaveletTransform :: Load r DIM2 Double => Int -> Img r -> Img U
inverseWaveletTransform 0 img = compS img
inverseWaveletTransform n img = inverseWaveletStep img'
    where topleft = inverseWaveletTransform (n-1) $ extractTopLeft img
          img'    = insertTopLeft img topleft

truncateCoeffs :: Source r Double => Double -> Img r -> Img D
truncateCoeffs threshold img = Repa.map f img
    where mean = sumAllS (Repa.map abs img) / (fromIntegral . size . extent $ img)
          f x | abs x < threshold*mean = 0
              | otherwise              = x

norm2 :: Source r Double => Img r -> Double
norm2 img = sumAllS . Repa.map (^(2::Int)) $ img

countZeros :: Source r Double => Img r -> Int
countZeros = round . foldAllS f 0
    where f acc x = if x == 0 then acc+1 else acc

main :: IO ()
main = do
  let n = 4
  image <- loadImage

  putStrLn $ "Number of elements:       "
               ++ show (size . extent $ image)

  let transformed = waveletTransform n . delay $ image

  putStrLn $ "Zeros in non-truncated:   "
               ++ show (countZeros transformed)

  let truncated = compS . truncateCoeffs 0.1 $ transformed

  putStrLn $ "Zeros in truncated:       "
               ++ show (countZeros truncated)
  putStrLn $ "Relative error in coeffs: "
               ++ show (sqrt (norm2 (truncated -^ transformed) / norm2 transformed))

  let compressed = inverseWaveletTransform n . delay $ truncated

  putStrLn $ "Relative error in image:  "
               ++ show (sqrt (norm2 (image -^ compressed) / norm2 image))
