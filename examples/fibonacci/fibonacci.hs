-- ghc  -O2 -prof -auto-all -rtsopts fibonacci.hs && time ./fibonacci +RTS -p -RTS
--import Control.Monad.Memo

fibs :: (Show a, Integral a) => a -> a
fibs n
     | n==0 || n==1 = 1
     | otherwise = fibs (n-1) + fibs (n-2)

fibsM :: Int -> Integer
fibsM n = (map fib [0 ..] !!) n
    where fib n 
            | n==0 || n==1 = 1
            | otherwise = fibsM (n-1) + fibsM (n-2)

modFibs :: (Show a, Integral a) => a -> a
modFibs a = tailFibs 0 1 0 a
tailFibs:: (Integral a, Show a) => a -> a -> a -> a -> a
tailFibs prev1 prev2 start end
    | start == end = next
    | otherwise = tailFibs next prev1 (start+1) end
    where next = prev1 + prev2

--fibm :: (Eq n, Num n, Ord n) => n -> Memo n n n
--fibm 0 = return 0
--fibm 1 = return 1
--fibm n = memo fibm (n-1) >>= \n1 -> (memo fibm (n-2)) >>= \n2 -> return (n1 + n2)

--fibm' :: (Num n, Ord n) => n -> n
--fibm' = startEvalMemo . fibm

main :: IO()
main = do
    let h = fibs 30
    print h
    let g = modFibs 30
    print g
    -- let i = fibsM 20
    --print i
    --let i = fibm' 20
    --print i
