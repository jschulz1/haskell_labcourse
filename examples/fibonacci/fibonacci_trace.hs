-- ghc  -O2 fibonacci_trace.hs && ./fibonacci_trace 
import Debug.HTrace

fibs :: (Show a, Integral a) => a -> a
fibs n | htrace ("fibs" ++ show n) False = undefined
fibs n
     | n==0 || n==1 = 1
     | otherwise = htrace "--" (fibs (htrace "1" (n-1)) + fibs (htrace "2" (n-2)))

modFibs :: (Show a, Integral a) => a -> a
modFibs a = tailFibs 0 1 0 a
tailFibs:: (Integral a, Show a) => a -> a -> a -> a -> a
tailFibs prev1 prev2 start end | htrace ("tailF" ++ show prev1 ++ " " ++ show prev2 ++ " " ++ show start ++ " " ++ show end) False = undefined
    | start == end = next
    | otherwise = htrace "---" (tailFibs (htrace "add" next) prev1 (htrace "+" (start+1)) end)
    where next = prev1 + prev2

main = do
    let h = fibs 5
    print h
    let g = modFibs 5
    print g
