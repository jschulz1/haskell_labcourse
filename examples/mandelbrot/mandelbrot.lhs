compile with

    ghc -Odph -rtsopts -threaded -fno-liberate-case -funfolding-use-threshold1000 -funfolding-keeness-factor1000 -fllvm -optlo-O3 -eventlog mandelbrot.lhs

run with (N is the number of threads)

    ./mandelbrot +RTS -s -N2 -l

and show what the threads are doing

    threadscope mandelbrot.eventlog

\begin{code}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BangPatterns #-}


import Data.Array.Repa as R
import Data.Array.Repa.Algorithms.Complex
import Data.List (findIndex)
import Data.Array.Repa.Algorithms.ColorRamp
import Data.Array.Repa.Algorithms.Pixel
import Data.Array.Repa.IO.BMP (writeImageToBMP)
import Control.Monad.Identity (runIdentity)


-- checks if sequence is infinite (and then returns the number of iterations it needed to determine that)
isGeneralMandel :: (Complex -> Complex -> Complex) -> Int -> Double -> Complex -> Maybe Int
isGeneralMandel f iteration_depth bound c = findIndex ((>bound) . mag) (take iteration_depth (iterate (f c) 0))

-- checks if given number is in mandelbrot set
isMandel :: Int -> Double -> Complex -> Maybe Int
isMandel =  isGeneralMandel (\c z -> z*z + c)

-- grid: values in complex plane
grid :: (Int, Int) -> DIM2
grid (x, y) = (Z :. x) :. y 

-- calculates the mesh of points in complex plane
calcView :: (Complex, Complex) -> (Int, Int) -> DIM2 -> Complex
calcView ((left, bottom), (right, top)) (max_x, max_y) (Z :. x :. y) = ((right - left) * (fromIntegral x)/(fromIntegral max_x - 1) + left, (top - bottom) * (fromIntegral y)/(fromIntegral max_y - 1) + bottom)

size = (2048,2048)
view = ((-1.5, -1.5),(1.5,1.5))

main :: IO ()
main = do 
    !coord <- computeP $ fromFunction (grid Main.size) ( calcView view Main.size) :: IO (Array U DIM2 Complex)

    (writeImageToBMP "../../images/mandel.bmp" . runIdentity . computeP) 
      $ R.map ( rgb8OfFloat. rampColorHotToCold 0 35 . maybe 0 fromIntegral . isMandel 35 3) coord 
    -- !a <- computeP $ R.map ( maybe 0 fromIntegral . isMandel 35 3) coord :: IO (Array U DIM2 Float) 
    print "finished"
\end{code}
