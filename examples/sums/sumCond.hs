sumCond :: Integral a => a -> (a -> Bool) -> a
sumCond limit funct = sum (takeWhile (<limit) (filter funct (map (^2) [1..])))

sumCondLC :: Integral a => a -> (a -> Bool) -> a
sumCondLC limit funct = sum (takeWhile (<limit) [n^2 | n <- [1..], funct (n^2)])
