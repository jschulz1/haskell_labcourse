-- ghc  -O2 -prof -auto-all -rtsopts sum.hs && ./sum +RTS -p -RTS
sumr :: Int -> Int
sumr n
    | n == 0    = 0
    | otherwise =  n +  sumr (n-1) 

sumtr :: Int -> Int
sumtr n = addSum 0 n 
addSum m n
      | n == 0      = m
      | otherwise   = addSum (m+n) (n-1)

main :: IO ()
main = do
    let a = sumtr 100000
    print a
    let b = sumr 100000
    print b
