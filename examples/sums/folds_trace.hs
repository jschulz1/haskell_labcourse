import Debug.HTrace
import Data.List

sumlTr :: Integer -> [Integer] -> Integer
sumlTr _ _ | htrace ("sumlTr " ) False = undefined
sumlTr x [] = x
sumlTr x (y:ys) = sumlTr (htrace "+" (x+y)) ys

sumlD :: [Int] -> Int
sumlD _ | htrace ("sumlD " ) False = undefined
sumlD [] = 0
sumlD (y:ys) = htrace "+" (y + sumlD ys)

mapm :: (a -> b) -> [a] -> [b]
mapm f xs = foldr (\x acc -> htrace "+" (f x) : acc) [] xs
-- mapm f xs = foldl (\acc x -> acc ++ [f x]) [] xs

foldlm :: (b -> a -> b) -> b -> [a] -> b
foldlm f z []     = z                  
foldlm f z (x:xs) = foldlm f (f z x) xs


main :: IO ()
main = do
    print "own tail recursion"
    let l1 = map (\x -> htrace (show x) x) [1..3]
    let a = sumlTr 0 l1
    print a
    print "own non tail recursion"
    let l2 = map (\x -> htrace (show x) x) [1..3]
    let b = sumlD l2
    print b
    print "foldl"
    let l3 = map (\x -> htrace (show x) x) [1..3]
    let c = foldl (\a b -> htrace "+" (a + b)) 0 l3
    print c
    print "foldl'"
    let l4 = map (\x -> htrace (show x) x) [1..3]
    let d = foldl' (\a b -> htrace "+" (a + b)) 0 l4
    print d
    print "foldr"
    let l5 = map (\x -> htrace (show x) x) [1..3]
    let e = foldr (\a b -> htrace "+" (a + b)) 0 l5
    print e
    let l6 = map (\x -> htrace (show x) x) [1..3]
    let f = mapm (+3) l6
    print f
