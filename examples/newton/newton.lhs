
compile with
    
    ghc -Odph -rtsopts -threaded -fno-liberate-case -funfolding-use-threshold1000 -funfolding-keeness-factor1000 -fllvm -optlo-O3 -eventlog newton.hs

run with (N is the number of threads)
    
    ./newton +RTS -s -N2 -l

and show what the threads are doing

    threadscope newton.eventlog 

\begin{code}
{-# Language BangPatterns #-}

import Data.Complex
import Data.Array.Repa as R
import Data.List (genericLength)
import Numeric.LinearAlgebra.Helpers
import Data.Array.Repa.IO.BMP (writeImageToBMP)
import Data.Array.Repa.Algorithms.ColorRamp
import Data.Array.Repa.Algorithms.Pixel
import Control.Monad.Identity (runIdentity)

newton :: (Complex Double -> Complex Double) -> Complex Double-> [(Complex Double,Complex Double)]
newton f xn = (f xn,xn) : newton f (fn xn)
    where fn xn = let h = 1e-11 in xn - f xn/((f (xn+h) - f xn)/h)

f !x = x**3 - 1

nm :: Complex Double -> (Double, Double)
nm !g = ((\x -> (phase . last $ x, genericLength x) ) . snd . unzip . takeWhile (\(y,_) -> magnitude y >1e-13) . newton f) g

n = 1000 -- size of the domain
coord = linspace n (-0.9,0.9::Double) --coordinates in one dimension
-- get complex values in a grid
m = fromFunction (Z :. n :. n) (\(Z :. x :. y) -> coord ! (ix1 x) :+ coord ! (ix1 y) ) 


main = do  
    -- create matrix of the mesh (and newton iteration start values)
    let coordm = computeUnboxedS m  
   
    -- solution of newton iteration for every startvalue
    let nv = R.map nm coordm
    solm <- computeUnboxedP nv
    let phasem = R.map fst solm
    -- number of iterations for every startvalue
    let numitm = R.map snd solm


    (writeImageToBMP "../../images/newton_konvergenz2.bmp" . runIdentity . computeP)
      $ R.map ( rgb8OfDouble. rampColorHotToCold 0 5 . (+2.5) ) phasem
    
    (writeImageToBMP "../../images/newton_conv_sol2.bmp" . runIdentity . computeP)
      $ R.map ( rgb8OfDouble. rampColorHotToCold 0 100) numitm

    print "finish"
\end{code}


