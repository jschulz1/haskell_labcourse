 # Minimal surfaces

 ## Task

Given a closed curve $\gamma$ in $\mathbb{R}^3$, determine the (smoothly
embedded) surface of minimal area that has $\gamma$ as its boundary.

 ## Formalities

- $\Omega \subset \mathbb{R}^2$ is the (open) parameter set with boundary
$\partial\Omega$.

- $f \colon \Omega \to \mathbb{R}^3$ is the embedding (i.e. parametrization),
its image $f(\Omega)$ is a surface in $\mathbb{R}^3$ with boundary
$f(\partial\Omega)$. (Technically, $f$ should be $C^2$).

- $Df \colon \Omega \to \mathbb{R}^{3 \times 2}$ is the Jacobian, i.e.

$$(Df(x))_{ij} = \frac{\partial f_i(x)}{\partial x_j}.$$

- $g = (Df)^T Df \colon \Omega \to \mathbb{R}^{2 \times 2}$ is the metric tensor
of the surface.

- The area of the surface $f(\Omega)$ is

$$A(f) = \int\limits_\Omega \sqrt{\det g(x)} \,dx.$$

- $\sqrt{\det g}$ is the *surface element*. We also write $\sqrt{\det g} \,dx =
d\sigma$.

We are searching for an $f$ such that $f(\partial\Omega) = \gamma$ and

$$A(f) = \min\left\{ A(\tilde{f}) \colon \tilde{f} \colon \Omega \to
\mathbb{R}^3, \tilde{f}(\partial\Omega) = \gamma \right\}$$

 ## Basic idea

Minimize $A$ using gradient descent:

$$f_{n+1} = f_n - \epsilon \nabla A(f_n), \qquad \epsilon: \text{ step size}$$

The derivative of $A$ at $f$ is

$$DA(f)(h) = \frac{\partial}{\partial t} A(f + th) |_{t = 0}$$

where $h \colon \Omega \to \mathbb{R}^3$ is an "infinitesimal shift" with
$h(\partial\Omega) = 0$ (i.e. not changing the boundary).

A tedious but simple calculation using [Jacobi's
formula](https://en.wikipedia.org/wiki/Jacobi%27s_formula) for the derivative of
the determinant shows

$$DA(f)(h) = \int\limits_\Omega \operatorname{tr}(g^{-1} (Df)^T Dh) \,d\sigma.$$

Partial integration (boundary terms vanish due to $h(\partial\Omega) = 0$) leads
to

$$DA(f)(h) = - \int\limits_\Omega \left( \sum_{ijk} \frac{1}{\sqrt{\det g}}
\frac{\partial}{\partial x_i} g^{-1}_{ij} \frac{\partial}{\partial x_j} f_k
\right) h_k \,d\sigma.$$

so (in a suitable sense), the gradient of $A$ is

$$\nabla A(f) = -\sum_{ijk} \frac{1}{\sqrt{\det g}} \frac{\partial}{\partial
x_i} g^{-1}_{ij} \frac{\partial}{\partial x_j} f = \Delta_{f(\Omega)} f$$

which is the *Laplace-Beltrami operator* of the surface $f(\Omega)$, applied to
$f$ itself.

 ## Imports

\begin{code}
import Control.Monad (zipWithM_, foldM)
import Data.Function (on)
import Data.List (foldl')
import qualified Data.Map.Strict as M
import Data.Maybe (fromJust)
import qualified Data.Set as S
import Numeric.Container ((<>), (<.>), norm2, buildMatrix)
import Numeric.LinearAlgebra.HMatrix hiding ((<>), CGState, cgx, cgp, cgr, cgr2)
\end{code}

 ## Discrete surfaces

Triangular mesh

\begin{code}
data Mesh = Mesh { vertices :: Matrix Double
                 , faces :: [(Int, Int, Int)]
                 }
            deriving Show
\end{code}

`vertices` is an $n \times 3$ matrix, each element of `faces` is a triple of
indices into the `points` matrix defining a triangle.

On the triangular mesh, define basis functions $\phi_p$ for every vertex $p$
(i.e. every row of `vertices`) with

- $\phi_p(p) = 1$

- $\phi_p(q) = 0$ for every vertex $q \not= p$

- $\phi_p$ is linear on each triangle

A triangular mesh is not a smooth surface. It can be shown, however, that the
gradient of the surface functional is still given by the (discrete)
Laplace-Beltrami operator, the matrix elements of which are $\Delta_{p q} = 0$
if $p$ is on the boundary of the mesh, and

$$\Delta_{p q} = - \sum_{T \text{ triangle}} \int\limits_T (\nabla \phi_p(x))^T
\nabla \phi_q(x) dx$$

otherwise. Gradient descent now becomes

$$\text{vertices}_{n+1} = \text{vertices}_n - \epsilon \Delta_n \cdot
\text{vertices}_n,$$

where $\Delta_n$ is the Lapalce operator on the mesh defined by
$\text{vertices}_n$.

 ## Computing the Laplace operator

Given vertices `x1`, `x2`, `x3`, compute the integral above for $p$ and $q$
running over these vertices (all other integrals vanish).

Some preliminaries:

- The three basis functions, taken as a map from the triangle to $\mathbb{R}^3$,
map bijectively onto the standard simplex

$$S = \left\{ x \in \mathbb{R}^3 \colon x \geq 0, \sum_i x_i = 1 \right\}$$

- The inverse of this map is just the matrix $(x1 \,\, x2 \,\, x3)$. On the
other hand, this provides a way of computing the basis functions as the inverse
of that matrix. (Note that it might be necessary to shift the triangle to ensure
the matrix is not singular).

- $\nabla \phi_p$ is constant on each triangle, since $\phi_p$ is linear. The
gradients of the basis of the functions in $\mathbb{R}^3$ are again simply given
by (the rows of) the matrix $(x1 \,\, x2 \,\, x3)^{-1}$.

- The *surface gradient* is the $\mathbb{R}^3$ gradient, projected onto the
triangle.

\begin{code}
laplaceForTriangle :: (Vector Double, Vector Double, Vector Double) -> Matrix Double
laplaceForTriangle (x1, x2, x3) = area `scale` (grads <> tr grads)
    where -- Area and surface normal of the triangle
          a = cross (x1 - x3) (x2 - x3)
          area = 1/2 * norm2 a
          normal = (1 / norm2 a) `scale` a
          -- Shift triangle to ensure non-singular matrix. All vertices are
          -- shifted by the same amount in normal direction. The choice of
          -- coefficient is a bit technical and not very interesting...
          x = fromColumns [x1, x2, x3]
          coeff = 1 - det x / (a <.> a)
          invBasis = x + coeff `scale` fromColumns [a, a, a]
          -- Compute basis functions
          -- basis = inv $ fromColumns [x1, x2, x3]
          basis = inv invBasis
          -- Projection onto triangle. Gradients are the rows of basis,
          -- so we project on the right.
          grads = basis - basis <> outer normal normal
\end{code}

Given a mesh, we want to compute the (sparse) Laplace operator. For this, we
need to construct an `AssocMatrix`. Recall its definition:

```haskell
type AssocMatrix = [((Int, Int), Double)]
```

First, turn the Laplace for a single triangle (given by a tuple of indices) into
an `AssocMatrix`:

\begin{code}
toAssocMatrix :: (Int, Int, Int) -> Matrix Double -> AssocMatrix
toAssocMatrix (i, j, k) mat = [ ((a, b), mat ! ai ! bi)
                              | (a, ai) <- l
                              , (b, bi) <- l ]
    where l = zip [i, j, k] [0, 1, 2]
\end{code}

The Laplace operator contains an element for every edge of the mesh. Since
triangles can share edges, we need a way to sum all elements of the
`AssocMatrix` that have identical indices.

Use a lookup table (`Data.Map`):

\begin{code}
uniquifyAssocMatrix :: AssocMatrix -> AssocMatrix
uniquifyAssocMatrix = M.assocs . foldl' ins M.empty
    where ins acc (k, v) = M.insertWith (+) k v acc
\end{code}

The Laplace is then computed by iterating over all faces. (Note that we do not
handle the boundary properly yet.)

\begin{code}
laplace :: Mesh -> AssocMatrix
laplace Mesh { vertices = vs, faces = fs } = uniquifyAssocMatrix elems
  where getTriangle (i, j, k) = (vs!i, vs!j, vs!k)
        elems = [ elm | face <- fs
                      , elm <- toAssocMatrix face . laplaceForTriangle $
                               getTriangle face ]
\end{code}

We also need a way to determine the boundary: boundary edges are the ones that
only belong to a single triangle.

\begin{code}
sortTuple :: Ord a => (a, a) -> (a, a)
sortTuple (i, j) = if i < j then (i, j) else (j, i)

edges :: Mesh -> [(Int, Int)]
edges mesh = [ sortTuple edge
             | (i, j, k) <- faces mesh
             , edge <- [(i, j), (j, k), (k, i)] ]

countOccurences :: Ord a => [a] -> [(a, Int)]
-- same idea as uniquifyAssocMatrix
countOccurences = M.assocs . foldl' ins M.empty . map (\x -> (x, 1))
    where -- This is the same ins as above, point-free
          ins = flip . uncurry $ M.insertWith (+)

boundary :: Mesh -> S.Set Int
boundary mesh = S.fromList [ idx
                           | ((i, j), count) <- countOccurences $ edges mesh
                           , count == 1
                           , idx <- [i, j]
                           ]
\end{code}

Finally, gradient descent can be implemented.

\begin{code}
-- Hmatrix does not provide products between a sparse and a full matrix.
-- This implementation here is not very efficient, but simple.
mult :: GMatrix -> Matrix Double -> Matrix Double
mult gm = fromColumns . map (gm !#>) . toColumns

gradientDescent :: Double -> Mesh -> [Mesh]
gradientDescent stepsize mesh = iterate step mesh
    where bndry = boundary mesh
          onBoundary ((i, _), _) = i `S.member` bndry
          step msh@Mesh { vertices = vs } =
              msh { vertices = vs - stepsize `scale` (lapl `mult` vs) }
              -- Sparse matrix would be nicer, but does not work: hmatrix tries
              -- to guess the size of the matrix, but gets it wrong sometimes:
              -- where lapl = mkSparse . filter (not . onBoundary) . laplace $ mesh
              where lapl = mkDense . toDense .
                           filter (not . onBoundary) . laplace $ msh
\end{code}

 ## Gradient Descent in $H^1$

Without proof: The method can be improved by taking the gradient in the Sobolev
space $H^1$, which leads to the iteration

$$\text{vertices}_{n+1} = \text{vertices}_n - \epsilon u_n.$$

where $u_n$ solves

$$\Delta_n u = \Delta_n \text{vertices}_n$$

on inner points, and

$$u = 0$$

on the boundary. We solve the equation using the Conjugate Gradient method.

\begin{code}
data CGState = CGState { cgx :: Matrix Double
                       , cgp :: Matrix Double
                       , cgr :: Matrix Double
                       , cgr2 :: Double }

cg :: GMatrix -> Matrix Double -> [CGState]
cg mat rhs = iterate cgStep (CGState zeros rhs rhs (rhs `mdot` rhs))
    where mdot = dot `on` flatten
          zeros = buildMatrix (rows rhs) (cols rhs) (const 0)
          cgStep (CGState x p r r2) = CGState x' p' r' r2'
              where matp = mat `mult` p
                    alpha = r2 / (p `mdot` matp)
                    x' = x + alpha `scale` p
                    r' = r - alpha `scale` matp
                    r2' = r' `mdot` r'
                    p' = r' + (r2' / r2) `scale` p
\end{code}

\begin{code}
gradientDescentH1 :: Double -> Mesh -> [Mesh]
gradientDescentH1 stepsize mesh = iterate step mesh
    where bndry = boundary mesh
          rowOnBoundary ((i, _), _) = i `S.member` bndry
          rowOrColOnBoundary ((i, j), _) = any (`S.member` bndry) [i, j]
          -- See `gradientDescent' for note on why mkSparse does not work
          mkLaplace pred = mkDense . toDense . filter pred
          cgStoppingRule state = cgr2 state < 1e-4
          step msh@Mesh { vertices = vs } =
              msh { vertices = vs - stepsize `scale` delta }
              where lapl0 = laplace msh
                    rhs = mkLaplace (not . rowOnBoundary) lapl0 `mult` vs
                    lapl = mkLaplace (not . rowOrColOnBoundary) lapl0
                    delta = cgx . head . filter cgStoppingRule $ cg lapl rhs
\end{code}

 ## Mesh construction

Starting from a simple, coarse base mesh, we iteratively construct finer meshes
by adding vertices in the center of all edges and splitting each triangle into 4
smaller ones.

\begin{code}
refineMesh :: Mesh -> Mesh
refineMesh mesh = Mesh { vertices = newvs, faces = newfs }
    where (newvs, mids) = foldl' addMidpoint (vertices mesh, M.empty) $
                          edges mesh
          addMidpoint (vs, ms) edge@(i, j) =
              let vs' = vs
                        ===
                        fromRows [(1/2) `scale` (vs!i + vs!j)]
                  ms' = M.insert (sortTuple edge) (rows vs) ms
              in (vs', ms')
          getMid edge = fromJust $ M.lookup (sortTuple edge) mids
          newfs = foldl' refineFace [] $ faces mesh
          refineFace fs (i, j, k) = let mi = getMid (j, k)
                                        mj = getMid (k, i)
                                        mk = getMid (i, j)
                                    in (i, mj, mk) :
                                       (mi, j, mk) :
                                       (mi, mj, k) :
                                       (mi, mj, mk) :
                                       fs

baseMesh :: Mesh
baseMesh =
    Mesh { vertices = (6><3) [ 0, 0, 0
                             , 1, 0, 0
                             , 1, 0, 1
                             , 1, 1, 1
                             , 0, 1, 1
                             , 0, 1, 0
                             ],
           faces = [ (0, 1, 5)
                   , (1, 2, 5)
                   , (2, 4, 5)
                   , (2, 3, 4)
                   ]
         }
\end{code}

 ## The main part

Save a mesh to files that can be read by the `vis.py` script. Call it like

    vis.py --type triangles --base <prefix>

where `<prefix>` is the `String` argument to `saveMesh`.

\begin{code}
saveMesh :: String -> Mesh -> IO ()
saveMesh prefix mesh = do
  zipWithM_ sav names columns
  writeFile (prefix ++ "-t.txt") triStr
    where sav fpath = saveMatrix fpath "%f"
          names = map (prefix ++) ["-x.txt", "-y.txt", "-z.txt"]
          columns = map asColumn $ toColumns $ vertices mesh

          fmtFace (i, j, k) = show i ++ " " ++ show j ++ " " ++ show k
          triStr = unlines $ map fmtFace $ faces mesh
\end{code}

Functions to process a list of meshs as generated by

```haskell
gradientDescent :: Double -> Mesh -> [Mesh]
```

Iterations are stopped when the distance between two meshs (in $L^2$-norm) are
below some tolerance. For each iteration, the distance to the preceding mesh is
printed.

\begin{code}
process :: Monad m => [a] -> (a -> m ()) -> m a
process xs f = foldM (\_ x -> f x >> return x) undefined xs

takeUntil :: (a -> Bool) -> [a] -> [a]
takeUntil _ [] = []
takeUntil predicate (x:xs)
    | predicate x = [x]
    | otherwise   = x : takeUntil predicate xs

processMeshs :: Double -> [Mesh] -> IO Mesh
processMeshs tol meshs = do
  let meshs' = takeUntil ((< tol) . snd)
              [ (cur, norm_Frob $ vertices prev - vertices cur)
              | (cur, prev) <- zip (tail meshs) meshs ]
  fmap (fst . snd) $ process (zip [(1::Int)..] meshs') $ \(n, (_, diff)) ->
      putStrLn $ show n ++ " " ++ show diff
\end{code}

\begin{code}
main :: IO ()
main = do
  let mesh = iterate refineMesh baseMesh !! 3
  let tol = 1e-2

  saveMesh "initial" mesh

  putStrLn "Gradient descent"
  processMeshs tol (gradientDescent 0.1 mesh) >>= saveMesh "minsurf"

  putStrLn "Gradient descent H1"
  processMeshs tol (gradientDescentH1 1 mesh) >>= saveMesh "minsurf-h1"
\end{code}
