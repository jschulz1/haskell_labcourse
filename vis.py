#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 14:14:05 2015

@author: jschulz1
"""

#import numpy as np  # NumPy (multidimensional arrays, linear algebra, ...)
#import matplotlib as mpl         # Matplotlib (2D/3D plotting library)
#import matplotlib.pyplot as plt  # Matplotlib's pyplot: MATLAB-like syntax
from mpl_toolkits.mplot3d import Axes3D
from mayavi import mlab as ml #majavi mlab
from pylab import *              # Matplotlib's pylab interface
import argparse

#command line options
p = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description="simple interface for plotting matrices in a surface")
p.add_argument('--base', '-b', help="Basename of the matrix files")
p.add_argument('--xmat', '-x', help="Filename of the x matrix (default: <base>-x.txt)")
p.add_argument('--ymat', '-y', help="Filename of the y matrix (default: <base>-y.txt)")
p.add_argument('--zmat', '-z', help="Filename of the z matrix (default: <base>-z.txt)")
p.add_argument('--tmat', '-r', help="Filename of the triangles matrix (default: <base>-t.txt). Only needed when <type> is 'triangles'.")
p.add_argument('--type', '-t', help="Type of plot",default="surf",choices=['surf','wireframe','contour','surf_contour','mayavi_surf','triangles','image'])
args = p.parse_args()

args.xmat = args.xmat or args.base + "-x.txt"
args.ymat = args.ymat or args.base + "-y.txt"
args.zmat = args.zmat or args.base + "-z.txt"
if args.type == 'triangles':
    args.tmat = args.tmat or args.base + "-t.txt"

#read required matrices
X = loadtxt(args.xmat, skiprows=2)
Y = loadtxt(args.ymat, skiprows=2)
Z = loadtxt(args.zmat, skiprows=2)
# and their dimensions
with open(args.xmat, 'r') as f:
    f.readline()
    header = f.readline()
(width,height) = header.split('\s')

X = X.reshape((int(width),int(height)))
Y = Y.reshape((int(width),int(height)))
Z = Z.reshape((int(width),int(height)))


from traits.api import HasTraits, Range, Instance, on_trait_change, Enum #Trait-types
from traitsui.api import View, Item, Group # traitsUI 
from tvtk.pyface.scene_editor import SceneEditor # scene Editor
from mayavi.tools.mlab_scene_model import MlabSceneModel # Scene Model
from mayavi.core.ui.mayavi_scene import MayaviScene # Scene

#creating class inheriting from the class HasTraits
class Visualization(HasTraits):
    vt = Enum ('Surf','Mesh','Contour','Image') # Enumeration
    scene = Instance(MlabSceneModel, ()) # creating scene-instance

    # initialization
    def __init__(self,xmat,ymat,zmat):
        # Do not forget to call the parent's __init__
        HasTraits.__init__(self)
        self.x = xmat
        self.y = ymat
        self.z = zmat        

        
    # when the scene is activated change the camera viewpoint    
    @on_trait_change('scene.activated')
    def create_plot(self):
        # create initial plot. self.scene.mlab is a complete mlab-reference
        self.plot = self.scene.mlab.surf(self.x,self.y,self.z,warp_scale='auto')
        #TODO: correct adding and removing of  colorbar and axes
        #self.scene.mlab.axes()
        #self.scene.mlab.scalarbar()
        self.scene.mlab.view(0,0)

    # when the user changes one of the 2 traits in the GUI, redraw the image.
    @on_trait_change('vt')
    def update_plot(self):
        self.plot.remove() # remove plot (to be able to recreate it)
        if self.vt == 'Surf':
            self.plot = self.scene.mlab.surf(self.x,self.y,self.z,warp_scale='auto')
        elif self.vt == 'Mesh':
            self.plot = self.scene.mlab.surf(self.x,self.y,self.z,representation='wireframe')
        elif self.vt == 'Contour':
            self.plot = self.scene.mlab.contour_surf(self.x,self.y,self.z,contours=15)
        elif self.vt == 'Image': 
            self.plot = self.scene.mlab.imshow(self.z)
            self.scene.mlab.view(0,0)
        else:
            print ("Plot-Auswahl fehlgeschlagen")
        

    # the layout of the dialog created
    view = View(Item('scene', editor=SceneEditor(scene_class=MayaviScene),
                    height=600, width=800, show_label=False), #1st Item
                Group( # 2nd item: horizontal Group for 'time' and 'vt'
                        'vt'
                    ,orientation='horizontal',layout='normal'),
                kind='live', title='simple GUI'
                )


if args.type == "wireframe":
    fig = figure(figsize=(16,12))
    ax = Axes3D(fig)
    ax.plot_wireframe(X,Y,Z)
    title('plot_wireframe')
    show()

if args.type == "surf":
    fig = figure(figsize=(16,12))
    ax = Axes3D(fig)
    ax.plot_surface(X,Y,Z,rstride=1,cstride=1,cmap=cm.jet,linewidth=0)
    title('plot_surf')
    show()

if args.type == "contour":
    contour(X,Y,Z,10)
    title('contour')
    show()

if args.type == "surf_contour":
    fig = figure(figsize=(16,12))
    ax = Axes3D(fig)
    ax.plot_surface(X,Y,Z,rstride=1,cstride=1,cmap=cm.jet)
    contour(X, Y, Z, zdir='z', offset=-0.5)
    view_init(20,-26)
    title('plot_surface+contour')
    #fig.savefig('figures/function_plot3d_py.pdf', format='PDF')
    show()

if args.type == "image":
    fig = figure(figsize=(16,12))
    imshow(Z,cmap=cm.jet)
    title('image')
    #fig.savefig('figures/function_plot3d_py.pdf', format='PDF')
    show()

    
    
if args.type == "triangles":
    triangles = loadtxt (args.tmat)
    ml.figure(bgcolor=(1,1,1)) # default bgcolor is black
    ml.triangular_mesh(X,Y,Z,triangles)
    title('plot_surf (mayavi)')
    ml.axes()
    ml.colorbar()
    ml.show()



if args.type ==  "mayavi_surf":
    # creating new object of the class
    visualization = Visualization(X,Y,Z)
    # starting GUI
    visualization.configure_traits()

    #ml.figure(bgcolor=(1,1,1)) # default bgcolor is black
    #ml.surf(X,Y,Z,warp_scale='auto')
    #title('plot_surf (mayavi)')
    #ml.axes()
    #ml.colorbar()
    #ml.show()

