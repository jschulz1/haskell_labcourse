{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises: boundary value problems\n",
    "\n",
    "## problem 1\n",
    "\n",
    "implement a *Gauss-Seidel* algorithm for solving linear equations.\n",
    "The Gauss\u2013Seidel method is an iterative technique for solving a square system of n linear equations with unknown x:\n",
    "\n",
    "$$A x = b$$\n",
    "\n",
    "It is defined by the iteration\n",
    "\n",
    "$$L_\u2217 x^{( k + 1 )} = b \u2212 U x^{( k )}$$ \n",
    "\n",
    "where $x^{( k )}$ is the $k$-th approximation or iteration of $x$ ,$x^{k + 1}$ is the next value of $x$ , and the matrix $A$ is decomposed into a lower triangular component $L_\u2217$ , and a strictly upper triangular component $U$: $A = L_\u2217 + U$\n",
    "\n",
    "We rewrite this a little bit to for the algorithm:\n",
    "\n",
    "$$x^{( k + 1 )} = T x^{( k )} + C$$ \n",
    "where \n",
    "$$T = - L_\u2217^{-1} U$$ and $$C = L_\u2217^{-1} b$$ \n",
    "\n",
    "Write a recursive function which makes this iteration. The iteration should stop both from a tolerance value of the difference of adjacent $x$ and a maximum of iterations. \n",
    "For a test case use the matrix and right hand side from the lecture."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "{-# LANGUAGE FlexibleContexts #-}\n",
    "\n",
    "import Numeric.LinearAlgebra.Repa\n",
    "import Numeric.LinearAlgebra.Helpers\n",
    "import qualified Data.Array.Repa as Repa\n",
    "import Data.Array.Repa hiding (map, (++))\n",
    "\n",
    "-- lets first create a matrix a and right hand side as in the lecture\n",
    "idxf (Z :. i :. j)\n",
    "   | i == j               = 2\n",
    "   | i == j-1 || i == j+1 = -1\n",
    "   | otherwise            = 0\n",
    "\n",
    "setupsystem :: Int -> (Mat Double, Vec Double, Vec Double)\n",
    "setupsystem n = (a,f,xi) \n",
    "    where\n",
    "        -- divide the interval\n",
    "        x :: Vec Double\n",
    "        x = computeS $ linspace n (0,1::Double)\n",
    "        -- cut of boundary points (they are zero anyway)\n",
    "        xi :: Vec Double\n",
    "        xi = subVector 1 98 x\n",
    "        -- build the matrix \n",
    "        a :: Mat Double\n",
    "        a = computeS $ fromFunction (ix2 (n-2) (n-2)) (idxf) :: Mat Double        \n",
    "        f :: Vec Double\n",
    "        f = computeS $ Repa.map (\\xi -> (1/(fromIntegral n))^2*exp(xi)) xi\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "gaussSeidel :: Mat Double -> Vec Double -> Int -> Double -> (Vec Double, Double)\n",
    "gaussSeidel a b tol = gsit (computeS $ ones (ix1 size)) 1 \n",
    "    where \n",
    "        n = size(extent b)\n",
    "        maxit = 20000\n",
    "        l = computeS $ fromFunction (ix2 (n-2) (n-2))\n",
    "                (\\(Z :. i :. j) -> if (i-j) <= 0 then a ! (ix2 i j) else 0 ) \n",
    "        u = computeS $ fromFunction (ix2 (n-2) (n-2))\n",
    "                (\\(Z :. i :. j) -> if (i-j) > 0 then a ! (ix2 i j) else 0 )\n",
    "        il = inv l\n",
    "        t = computeS (Repa.map negate il) `mul` u\n",
    "        c = il `app` b \n",
    "        gsit x it\n",
    "            | it > maxit || err < tol = (x,err)\n",
    "            | otherwise = gsit xn (it+1) \n",
    "            where \n",
    "                xn :: Vec Double\n",
    "                xn = computeS $ (t `app` x) +^ c\n",
    "                err :: Double\n",
    "                err = norm2 $ computeS (xn-^x)\n",
    "\n",
    "tol = 1e-8\n",
    "n = 100\n",
    "(a,b,x) = setupsystem n\n",
    "(sol,_) = gaussSeidel a b n tol\n",
    "\n",
    "import Graphics.Rendering.Chart.Easy (toRenderable, layout_title, plot, line, (.=))\n",
    "import Graphics.Rendering.Chart.Backend.Cairo ()\n",
    "\n",
    "toRenderable $ do \n",
    "    layout_title .= \"2-point boundary value problem\"\n",
    "    plot (line \"sol\" [zip (toList x) (toList sol)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## problem 2\n",
    "\n",
    "Write an implementation of the *conjugate gradient* (CG) method. This method solves\n",
    "\n",
    "$$A x = b$$\n",
    "\n",
    "by an iteration of the form $x_0 = 0$, $r_0 = p_0 = b$,\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "\\alpha_k &= \\frac{{\\lVert r_k \\rVert}^2}{\\langle p_k, A p_k \\rangle} \\\\\n",
    "x_{k+1} &= x_k + \\alpha_k p_k \\\\\n",
    "r_{k+1} &= r_k - \\alpha_k A p_k \\\\\n",
    "p_{k+1} &= r_{k+1} + \\frac{{\\lVert r_{k+1} \\rVert}^2}{{\\lVert r_k \\rVert}^2} p_k\n",
    "\\end{align},\n",
    "$$\n",
    "\n",
    "stopping the iterations at the smallest $K$ for which $\\lVert r_K \\rVert < \\text{tol}$.\n",
    "\n",
    "An interesting property of this method in contrast to the ones from the previous problems is that it does not need $A$ in form of a matrix; it suffices to be able to apply $A$ to any given vector $p$, so $A$ can also be passed as a function `Vec Double -> Vec Double`.\n",
    "\n",
    "The CG implementation should therefore have a signature like this:\n",
    "\n",
    "```haskell\n",
    "cg :: Double -> (Vec Double -> Vec Double) -> Vec Double -> Vec Double\n",
    "cg tol f rhs = [...]\n",
    "```\n",
    "\n",
    "Apply the method to the same test case as above, but this time reimplement it using the function\n",
    "\n",
    "```haskell\n",
    "conv :: Vec t -> Vec t -> Vec t\n",
    "```\n",
    "\n",
    "from `Numeric.LinearAlgebra.Repa`, which implements a convolution without actually creating the matrix. Note that `conv` returns a vector that is larger than the input; use the boundary values to handle this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data CGState = CGState { cgx :: Vec Double\n",
    "                       , cgp :: Vec Double\n",
    "                       , cgr :: Vec Double\n",
    "                       , cgr2 :: Double }\n",
    "\n",
    "cg' :: (Vec Double -> Vec Double) -> Vec Double -> [CGState]\n",
    "cg' f rhs = iterate cgStep (CGState zrs rhs rhs (rhs `dot` rhs))\n",
    "    where zrs = computeS $ zeros (ix1 $ vlength rhs)\n",
    "          cgStep (CGState x p r r2) = CGState x' p' r' r2'\n",
    "              where q = f p\n",
    "                    alpha = r2 / (p `dot` q)\n",
    "                    x' = computeS $ x +^ alpha `scale` p\n",
    "                    r' :: Vec Double\n",
    "                    r' = computeS $ r -^ alpha `scale` q\n",
    "                    r2' = r' `dot` r'\n",
    "                    p' = computeS $ r' +^ (r2' / r2) `scale` p\n",
    "\n",
    "takeUntil :: (a -> Bool) -> [a] -> [a]\n",
    "takeUntil _ [] = []\n",
    "takeUntil predicate (x:xs)\n",
    "    | predicate x = [x]\n",
    "    | otherwise   = x : takeUntil predicate xs\n",
    "\n",
    "cg :: Double -> (Vec Double -> Vec Double) -> Vec Double -> Vec Double\n",
    "cg tol f rhs = cgx . last $ takeUntil stoprule steps\n",
    "    where steps = cg' f rhs\n",
    "          stoprule = \\cgs -> sqrt (cgr2 cgs) < tol"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Graphics.Rendering.Chart.Easy (toRenderable, layout_title, plot, line, (.=))\n",
    "import Graphics.Rendering.Chart.Backend.Cairo ()\n",
    "\n",
    "sndDeriv :: Double -> Vec Double -> Vec Double\n",
    "sndDeriv h x = subVector 1 (vlength x) $ conv kernel x\n",
    "    where kernel = computeS $ (1/h^2) `scale` vec [-1, 2, -1]\n",
    "\n",
    "xs :: Vec Double\n",
    "xs = computeS $ linspace 100 (0, 1)\n",
    "\n",
    "rhs :: Vec Double\n",
    "rhs = computeS $ Repa.map exp xs\n",
    "\n",
    "h :: Double\n",
    "h = (xs!(ix1 1)) - (xs!(ix1 0))\n",
    "\n",
    "sol :: Vec Double\n",
    "sol = cg 1e-3 (sndDeriv h) rhs\n",
    "\n",
    "toRenderable $ do \n",
    "    layout_title .= \"2-point boundary value problem\"\n",
    "    plot (line \"sol\" [zip (toList xs) (toList sol)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## problem 3\n",
    "\n",
    "\n",
    "Let A be a $20.000 \\times 20.000$ matrix, which entries are all zero besides the prime numbers $2, 3, 5, 7, \\ldots, 224737$ on the diagonal and the number 1 in all entries $a_{ij}$ with \n",
    "$$|i \u2212 j| = 1, 2, 4, 8, \\ldots , 16384.$$\n",
    "\n",
    "What is the $(1, 1)$ entry of $A^{\u22121}$? How can you check/visualize the structure of the matrix? Be sure to use a good method for saving the matrix.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Data.Array.Unboxed hiding ((!))\n",
    " \n",
    "primes :: [Int]\n",
    "primes = 2 : oddprimes ()\n",
    "  where \n",
    "    oddprimes () = 3 : sieve (oddprimes ()) 3 []\n",
    "    sieve (p:ps) x fs = [i*2 + x | (i,True) <- assocs a] \n",
    "                        ++ sieve ps (p*p) ((p,0) : \n",
    "                             [(s, rem (y-q) s) | (s,y) <- fs])\n",
    "     where\n",
    "      q = (p*p-x)`div`2\n",
    "      a :: UArray Int Bool\n",
    "      a = accumArray (\\ b c -> False) True (1,q-1)\n",
    "                     [(i,()) | (s,y) <- fs, i <- [y+s, y+s+s..q]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Data.Array.Repa hiding (map, (++))\n",
    "import Numeric.LinearAlgebra.Helpers\n",
    "import Numeric.LinearAlgebra.Repa\n",
    "\n",
    "size = 1000\n",
    "a' = fromFunction (ix2 size size) f\n",
    "  where\n",
    "    f (Z :. i :. j)\n",
    "        | i == j         = fromIntegral (ps !! i)\n",
    "        | is2x(abs(i-j)) = 1\n",
    "        | otherwise      = 0\n",
    "    ps = take size primes\n",
    "    \n",
    "is2x x = let l = logBase 2 (fromIntegral x)\n",
    "    in 0.0000001 > abs (l - fromIntegral (round l))\n",
    "--map is2x [1..8]\n",
    "\n",
    "a = computeS a' :: Mat Double\n",
    "--a\n",
    "\n",
    "-- now we think of Ax = b where b = e_1, then x_1 = A^-1_1,1\n",
    "b = computeS $ fromFunction (ix1 size) (\\(Z :. i) -> if i == 1 then 1 else 0) \n",
    "-- x = a <\\> b  -- TODO: solve is too slow, use cg\n",
    "-- x = cg 1e-3 a (column 1 b )\n",
    "tol = 1e-8\n",
    ":ty b\n",
    "(x,_) = gaussSeidel a b size tol\n",
    "\n",
    "x ! ix1 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    ":ty column"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
