{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises: inverse problems\n",
    "\n",
    "The *Volterra operator* $V$ is defined by\n",
    "\n",
    "$$V f(x) = \\int_{x_0}^x f(x) \\,dx.$$\n",
    "\n",
    "A simple discrete approximation is\n",
    "\n",
    "$$(V f)_i = h \\sum_{j=0}^i f_j,$$\n",
    "\n",
    "where $f_j = f(x_j)$ are the values of $f$ at equidistant sampling points $(x_j)_{j=1}^{N-1}$ with distance $h = x_1 - x_0$. As a matrix, this can simply be written as\n",
    "\n",
    "$$V = h \\begin{pmatrix} \n",
    "  1 & 0 & 0 & 0 & \\ldots & 0 \\\\\n",
    "  1 & 1 & 0 & 0 & \\ldots & 0 \\\\\n",
    "  1 & 1 & 1 & 0 & \\ldots & 0 \\\\\n",
    "  \\vdots & & & \\ddots & & \\\\\n",
    "  1 & 1 & 1 & 1 & \\ldots & 1\n",
    "\\end{pmatrix}$$\n",
    "\n",
    "- Implement the Volterra operator with parameter `h :: Double` and its adjoint.\n",
    "\n",
    "    Though it is possible to do this using a matrix formulation and e.g. `mmultS`, this unneccessarily inefficient. A better possibility is to note the connection between the Volterra operator and Haskell's `scanl` function. Moreover, there is an implementation of `scanl` for unboxed `Vector`s from `Data.Vector.Unboxed` as well as O(1) conversion functions `toUnboxed` and `fromUnboxed` for Repa `U` arrays to and from these `Vector`s. Use these to implement the operator.\n",
    "    \n",
    "    *Note*: The adjoint (i.e. transpose) of the operator can be found most easily by noting the similarity between the Volterra matrix above and its transpose: the adjoint simply proceeds in the opposite direction.\n",
    "\n",
    "- Use the regularized CG implementation from the lecture to compute regularized *derivatives* of the functions\n",
    "    $$\\begin{align}\n",
    "    f_1(x) &= e^{-x^2} \\\\\n",
    "    f_2(x) &= \\text{sgn}(x) = \\begin{cases} -1 & x < 0 \\\\ 1 & x \\geq 0 \\end{cases}\n",
    "    \\end{align}$$\n",
    "    sampled at 200 points between -2 and 2 and distorted by random noise between -0.1 and 0.1. The CG tolerance should be set suffieciently low (e.g. 1e-6).\n",
    "    \n",
    "- Plot the results together with the noisy functions and, in the case of $f_1$, the analytical derivative."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "{-# LANGUAGE BangPatterns #-}\n",
    "\n",
    "import Control.Monad.Identity\n",
    "import Data.Array.Repa as R\n",
    "import qualified Data.Vector.Unboxed as V\n",
    "import Data.Array.Repa.Helpers (runCGreg)\n",
    "import Data.Random.Normal (normalsIO')\n",
    "import Numeric.LinearAlgebra.Helpers (linspace)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "vec2arr :: Shape sh => (V.Vector Double -> V.Vector Double) -> Array U sh Double -> Array U sh Double\n",
    "vec2arr f arr = fromUnboxed (extent arr) . f . toUnboxed $ arr\n",
    "\n",
    "volterra :: Double -> Array U DIM1 Double -> Array U DIM1 Double\n",
    "volterra h = vec2arr $ V.postscanl' (\\s x -> s+h*x) 0\n",
    "\n",
    "volterraAdjoint :: Double -> Array U DIM1 Double -> Array U DIM1 Double\n",
    "volterraAdjoint h = vec2arr $ V.postscanr' (\\x s -> s+h*x) 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "-- This was not required by the exercise, but is nice to have: test the\n",
    "-- implementation of the adjoint by checking whether y^T A x = (A^T y)^T x\n",
    "-- is fulfilled numerically for 50 random vectors of a given dimension.\n",
    "\n",
    "-- Note that randomishDoubleArray is yet another way to generate random numbers.\n",
    "\n",
    "import Data.Array.Repa.Algorithms.Randomish\n",
    "\n",
    "testAdjoint :: (Array U DIM1 Double -> Array U DIM1 Double) -> (Array U DIM1 Double -> Array U DIM1 Double) -> DIM1 -> Double\n",
    "testAdjoint operator adjoint shape = maximum $ Prelude.map testAdjoint' [1..50]\n",
    "    where testAdjoint' seed = abs $ sumAllS (y *^ fx) - sumAllS (fty *^ x)\n",
    "            where x = randomishDoubleArray shape (-1) 1 seed\n",
    "                  fx = operator x\n",
    "                  y = randomishDoubleArray (extent fx) (-1) 1 (seed+1)\n",
    "                  fty = adjoint y\n",
    "\n",
    "testAdjoint (volterra 1) (volterraAdjoint 1) (Z:.100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "-- The function performing the main computations; returns the noisy function and\n",
    "-- the regularizied solution for given function, sample points, noise level and\n",
    "-- regularization parameter.\n",
    "\n",
    "doDeriv :: (Double -> Double) -> Array U DIM1 Double -> Double -> Double -> IO (Array U DIM1 Double, Array U DIM1 Double)\n",
    "doDeriv f xs noiselevel reg = do\n",
    "    let func = computeUnboxedS $ R.map f xs\n",
    "    let noise = randomishDoubleArray (extent func) (-noiselevel) noiselevel 0\n",
    "    let rhs = computeUnboxedS $ func +^ noise\n",
    "    let h = (xs ! (Z:.1)) - (xs ! (Z:.0))\n",
    "    print h\n",
    "    deriv <- runCGreg 1e-6 (volterra h) (volterraAdjoint h) reg rhs\n",
    "    return (rhs, deriv)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Graphics.Rendering.Chart.Easy\n",
    "import Graphics.Rendering.Chart.Backend.Cairo\n",
    "\n",
    "xs :: Array U DIM1 Double\n",
    "xs = computeS $ linspace 200 (-2, 2)\n",
    "\n",
    "exact = computeUnboxedS $ R.map (\\x -> -2*x*exp(-x^2)) xs\n",
    "\n",
    "(rhs, deriv) <- doDeriv (\\x -> exp (-x^2)) xs 0.1 0.07\n",
    "\n",
    "toRenderable $ do\n",
    "    plot (line \"noisy\" [zip (toList xs) (toList rhs)])\n",
    "    plot (line \"exact\" [zip (toList xs) (toList exact)])\n",
    "    plot (line \"deriv\" [zip (toList xs) (toList deriv)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "(rhs, deriv) <- doDeriv signum xs 0.1 0.05\n",
    "\n",
    "toRenderable $ do\n",
    "    plot (line \"noisy\" [zip (toList xs) (toList rhs)])\n",
    "    plot (line \"derivative\" [zip (toList xs) (toList deriv)])"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
