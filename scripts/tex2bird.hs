import Data.List.Split (splitWhen)
import Data.List (isPrefixOf)

(#) :: (a -> b) -> (b -> c) -> a -> c
(#) = flip (.)
infixr 1 #

beginend :: String -> Bool
beginend s = isPrefixOf "\\begin{code}" s || isPrefixOf "\\end{code}" s

tex2bird :: String -> String
tex2bird = ("> " ++)

main :: IO ()
main = interact $
       lines #
       splitWhen beginend #
       zipWith ($) (cycle [id, map tex2bird]) #
       concat #
       unlines
