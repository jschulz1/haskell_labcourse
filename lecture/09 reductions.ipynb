{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reductions (folds)\n",
    "\n",
    "\n",
    "- Imperative languages have looping constructs to iterate over lists (e.g. `for`): execute the same code, with the loop variable taking a new value each time.\n",
    "- The loop body is necessarily effectful $\\Rightarrow$ not useful in Haskell.\n",
    "- Instead, *folds* iterate over lists, passing an explicit accumulator variable around to maintain referential transparency.\n",
    "\n",
    "## Left fold\n",
    "\n",
    "```haskell\n",
    "foldl :: (a -> b -> a) -> a -> [b] -> a\n",
    "foldl stepFunction initial list = ...\n",
    "```\n",
    "\n",
    "`a` is the type of the accumulator, `stepFunction` is a function mapping the accumulator and a list element into the new accumulator. The second argument is the initial accumulator.\n",
    "\n",
    "`foldl` essentially puts `f` in between list elements and reduces from left to right:\n",
    "\n",
    "```haskell\n",
    "foldl f z [x1, x2, x3, .., xn]\n",
    "==> (((z `f` x1) `f` x2) `f` x3) .. `f` xn\n",
    "```\n",
    "\n",
    "## Right fold\n",
    "\n",
    "```haskell\n",
    "foldr :: (b -> a -> a) -> a -> [b] -> a\n",
    "```\n",
    "\n",
    "Here, the step function takes the accumulator as the second argument. `foldr` reduces right to left:\n",
    "\n",
    "```haskell\n",
    "foldr f z [x1, x2, x3, .., xn]\n",
    "==> x1 `f` (x2 `f` (x3 `f` .. (xn `f` z)))\n",
    "```\n",
    "\n",
    "## Examples\n",
    "\n",
    "### Sum of a list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    ":option no-lint"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "55"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "foldl (+) 0 [1..10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`foldr` yields the same result, since `(+)` is associative"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "55"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "foldr (+) 0 [1..10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implementing `map` using folds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1,4,9,16,25]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mapr :: (a -> b) -> [a] -> [b]\n",
    "mapr f xs = foldr (\\x acc -> f x : acc) [] xs\n",
    "-- even shorter: mapr f = foldr ((:) . f) []\n",
    "\n",
    "mapr (^2) [1..5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1,4,9,16,25]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mapl :: (a -> b) -> [a] -> [b]\n",
    "mapl f xs = foldl (\\acc x -> acc ++ [f x]) [] xs\n",
    "\n",
    "mapl (^2) [1..5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- `mapr` is more efficient, since `(++)` is expensive.\n",
    "- `mapr` can handle infinite lists, `mapl` can not (see below):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1,4,9,16,25,36,49,64,81,100]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "take 10 $ mapr (^2) [1..]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Strictness and building up thunks\n",
    "\n",
    "- `foldr` is \"more lazy\" than `foldl`: if `f` is (partially) lazy, e.g. the list constructor `(:)` in `mapr`, then\n",
    "\n",
    "    ```haskell\n",
    "    x1 `f` (x2 `f` (x3 `f` .. ))\n",
    "    ```\n",
    "\n",
    "    can return directly after evaluating sufficiently many terms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "foldr (&&) True (repeat False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- On the other hand, if `f` is not lazy, the outermost call to `f` can only be performed after the rest of the list has been reduced.\n",
    "\n",
    "- Even for lazy `f`, the left fold\n",
    "\n",
    "    ```haskell\n",
    "    (((z `f` x1) `f` x2) `f` x3 ..)\n",
    "    ```\n",
    "\n",
    "    needs to traverse the whole list:\n",
    "    \n",
    "    ```haskell\n",
    "    foldl (&&) False (repeat False)\n",
    "    ```\n",
    "    \n",
    "    will not terminate\n",
    "    \n",
    "- On the other hand, `foldl` traverses the list \"in the right direction\" (i.e. front to back, corresponding to the list constructor `(:)`). So the first call to `f` in ``(z `f` x1)`` can be performed directly, without touching the rest of the list.\n",
    "\n",
    "- So for strict `f`, a left fold is *potentially* more efficient.\n",
    "\n",
    "- **Except it isn't**: `foldl` *itself* is lazy, i.e. does not actually evaluate ``(z `f` x1)`` until needed at the very end. Instead, it builds up *thunks* $\\Rightarrow$ inefficient.\n",
    "\n",
    "- Remedy (for simple cases): `foldl'`, a strict version of `foldl`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Data.List"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>/* Styles used for the Hoogle display in the pager */\n",
       ".hoogle-doc {\n",
       "display: block;\n",
       "padding-bottom: 1.3em;\n",
       "padding-left: 0.4em;\n",
       "}\n",
       ".hoogle-code {\n",
       "display: block;\n",
       "font-family: monospace;\n",
       "white-space: pre;\n",
       "}\n",
       ".hoogle-text {\n",
       "display: block;\n",
       "}\n",
       ".hoogle-name {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-head {\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-sub {\n",
       "display: block;\n",
       "margin-left: 0.4em;\n",
       "}\n",
       ".hoogle-package {\n",
       "font-weight: bold;\n",
       "font-style: italic;\n",
       "}\n",
       ".hoogle-module {\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-class {\n",
       "font-weight: bold;\n",
       "}\n",
       ".get-type {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "font-family: monospace;\n",
       "display: block;\n",
       "white-space: pre-wrap;\n",
       "}\n",
       ".show-type {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "font-family: monospace;\n",
       "margin-left: 1em;\n",
       "}\n",
       ".mono {\n",
       "font-family: monospace;\n",
       "display: block;\n",
       "}\n",
       ".err-msg {\n",
       "color: red;\n",
       "font-style: italic;\n",
       "font-family: monospace;\n",
       "white-space: pre;\n",
       "display: block;\n",
       "}\n",
       "#unshowable {\n",
       "color: red;\n",
       "font-weight: bold;\n",
       "}\n",
       ".err-msg.in.collapse {\n",
       "padding-top: 0.7em;\n",
       "}\n",
       ".highlight-code {\n",
       "white-space: pre;\n",
       "font-family: monospace;\n",
       "}\n",
       ".suggestion-warning { \n",
       "font-weight: bold;\n",
       "color: rgb(200, 130, 0);\n",
       "}\n",
       ".suggestion-error { \n",
       "font-weight: bold;\n",
       "color: red;\n",
       "}\n",
       ".suggestion-name {\n",
       "font-weight: bold;\n",
       "}\n",
       "</style><div class=\"suggestion-name\" style=\"clear:both;\">Use sum</div><div class=\"suggestion-row\" style=\"float: left;\"><div class=\"suggestion-warning\">Found:</div><div class=\"highlight-code\" id=\"haskell\">foldr (+) 0</div></div><div class=\"suggestion-row\" style=\"float: left;\"><div class=\"suggestion-warning\">Why Not:</div><div class=\"highlight-code\" id=\"haskell\">sum</div></div>"
      ],
      "text/plain": [
       "Line 1: Use sum\n",
       "Found:\n",
       "foldr (+) 0\n",
       "Why not:\n",
       "sum"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "50000005000000"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "foldr (+) 0 [1..10000000]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>/* Styles used for the Hoogle display in the pager */\n",
       ".hoogle-doc {\n",
       "display: block;\n",
       "padding-bottom: 1.3em;\n",
       "padding-left: 0.4em;\n",
       "}\n",
       ".hoogle-code {\n",
       "display: block;\n",
       "font-family: monospace;\n",
       "white-space: pre;\n",
       "}\n",
       ".hoogle-text {\n",
       "display: block;\n",
       "}\n",
       ".hoogle-name {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-head {\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-sub {\n",
       "display: block;\n",
       "margin-left: 0.4em;\n",
       "}\n",
       ".hoogle-package {\n",
       "font-weight: bold;\n",
       "font-style: italic;\n",
       "}\n",
       ".hoogle-module {\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-class {\n",
       "font-weight: bold;\n",
       "}\n",
       ".get-type {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "font-family: monospace;\n",
       "display: block;\n",
       "white-space: pre-wrap;\n",
       "}\n",
       ".show-type {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "font-family: monospace;\n",
       "margin-left: 1em;\n",
       "}\n",
       ".mono {\n",
       "font-family: monospace;\n",
       "display: block;\n",
       "}\n",
       ".err-msg {\n",
       "color: red;\n",
       "font-style: italic;\n",
       "font-family: monospace;\n",
       "white-space: pre;\n",
       "display: block;\n",
       "}\n",
       "#unshowable {\n",
       "color: red;\n",
       "font-weight: bold;\n",
       "}\n",
       ".err-msg.in.collapse {\n",
       "padding-top: 0.7em;\n",
       "}\n",
       ".highlight-code {\n",
       "white-space: pre;\n",
       "font-family: monospace;\n",
       "}\n",
       ".suggestion-warning { \n",
       "font-weight: bold;\n",
       "color: rgb(200, 130, 0);\n",
       "}\n",
       ".suggestion-error { \n",
       "font-weight: bold;\n",
       "color: red;\n",
       "}\n",
       ".suggestion-name {\n",
       "font-weight: bold;\n",
       "}\n",
       "</style><div class=\"suggestion-name\" style=\"clear:both;\">Use sum</div><div class=\"suggestion-row\" style=\"float: left;\"><div class=\"suggestion-warning\">Found:</div><div class=\"highlight-code\" id=\"haskell\">foldl (+) 0</div></div><div class=\"suggestion-row\" style=\"float: left;\"><div class=\"suggestion-warning\">Why Not:</div><div class=\"highlight-code\" id=\"haskell\">sum</div></div>"
      ],
      "text/plain": [
       "Line 1: Use sum\n",
       "Found:\n",
       "foldl (+) 0\n",
       "Why not:\n",
       "sum"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "50000005000000"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "foldl (+) 0 [1..10000000]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "50000005000000"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "foldl' (+) 0 [1..10000000]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** Strict means (roughly) to evaluate arguments *up to data constructors*. Strict evaluation of the *thunk* `{(1+2, 3+4)}` (here {} denotes the unevaluated part) does not yield `(3, 7)`, but `({1+2}, {3+4})`, i.e. a *tuple of thunks*. Evaluation is only up to the tuple constructor `(,)`.\n",
    "\n",
    "*Example*: average of a list of numbers by using `foldl'` to compute the sum and length while traversing the list only once:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4.5"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "avg xs = s / l\n",
    "    where (s, l) = foldl' (\\(s', l') x -> (s'+x, l'+1)) (0, 0) xs\n",
    "    \n",
    "avg [3, 4, 5, 6]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Despite using `foldl'`, the reduction builds up *thunks* inside the tuple. To avoid that, one can use `seq` to force evaluation of `s'` and `l'`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "avg xs = s / l\n",
    "    where (s, l) = foldl' (\\(s', l') x -> s' `seq` l' `seq` (s'+x, l'+1)) (0, 0) xs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, *BangPatterns* can be used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "{-# LANGUAGE BangPatterns #-}\n",
    "avg xs = s / l\n",
    "    where (s, l) = foldl' (\\(!s', !l') x -> (s'+x, l'+1)) (0, 0) xs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More on `seq` and `BangPatterns` $\\Rightarrow$ later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rule of thumb\n",
    "\n",
    "- Use `foldl'` for strict operations.\n",
    "- Otherwise, use `foldr`.\n",
    "- Don't use `foldl`.\n",
    "\n",
    "## Variation: take init value from list itself\n",
    "\n",
    "```haskell\n",
    "foldr1, foldl1, foldl1' :: (a -> a -> a) -> [a] -> a\n",
    "```\n",
    "\n",
    "Effectively,\n",
    "\n",
    "```haskell\n",
    "foldl1 f xs = foldl f (head xs) (tail xs)\n",
    "```\n",
    "\n",
    "and similarly for `foldr1`. Beware of empty lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>/* Styles used for the Hoogle display in the pager */\n",
       ".hoogle-doc {\n",
       "display: block;\n",
       "padding-bottom: 1.3em;\n",
       "padding-left: 0.4em;\n",
       "}\n",
       ".hoogle-code {\n",
       "display: block;\n",
       "font-family: monospace;\n",
       "white-space: pre;\n",
       "}\n",
       ".hoogle-text {\n",
       "display: block;\n",
       "}\n",
       ".hoogle-name {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-head {\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-sub {\n",
       "display: block;\n",
       "margin-left: 0.4em;\n",
       "}\n",
       ".hoogle-package {\n",
       "font-weight: bold;\n",
       "font-style: italic;\n",
       "}\n",
       ".hoogle-module {\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-class {\n",
       "font-weight: bold;\n",
       "}\n",
       ".get-type {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "font-family: monospace;\n",
       "display: block;\n",
       "white-space: pre-wrap;\n",
       "}\n",
       ".show-type {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "font-family: monospace;\n",
       "margin-left: 1em;\n",
       "}\n",
       ".mono {\n",
       "font-family: monospace;\n",
       "display: block;\n",
       "}\n",
       ".err-msg {\n",
       "color: red;\n",
       "font-style: italic;\n",
       "font-family: monospace;\n",
       "white-space: pre;\n",
       "display: block;\n",
       "}\n",
       "#unshowable {\n",
       "color: red;\n",
       "font-weight: bold;\n",
       "}\n",
       ".err-msg.in.collapse {\n",
       "padding-top: 0.7em;\n",
       "}\n",
       ".highlight-code {\n",
       "white-space: pre;\n",
       "font-family: monospace;\n",
       "}\n",
       ".suggestion-warning { \n",
       "font-weight: bold;\n",
       "color: rgb(200, 130, 0);\n",
       "}\n",
       ".suggestion-error { \n",
       "font-weight: bold;\n",
       "color: red;\n",
       "}\n",
       ".suggestion-name {\n",
       "font-weight: bold;\n",
       "}\n",
       "</style><span class='err-msg'>Prelude.foldl1: empty list</span>"
      ],
      "text/plain": [
       "Prelude.foldl1: empty list"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "foldl1 (+) []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scans\n",
    "\n",
    "`scanl` and `scanr` are similar to `foldl` and `foldr`, but retain intermediate accumulators:\n",
    "\n",
    "```haskell\n",
    "scanl :: (a -> b -> a) -> a -> [b] -> [a]\n",
    "scanr :: (b -> a -> a) -> a -> [b] -> [a]\n",
    "```\n",
    "\n",
    "- `scanl` yields results in the same order as the list.\n",
    "- `scanr` yields results in reverse order.\n",
    "- `scanl` can handle infinte lists, since it only returns the list of *intermediate* `foldl`s for the finite sublists.\n",
    "\n",
    "*Examples*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0,1,3,6,10,15]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "scanl (+) 0 [1..5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[15,14,12,9,5,0]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "scanr (+) 0 [1..5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0,1,3,6,10,15,21,28,36,45]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "take 10 $ scanl (+) 0 [1..]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More information: [Folds on the HaskellWiki](http://www.haskell.org/haskellwiki/Fold)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
