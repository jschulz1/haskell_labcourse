{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Floyd-Warshall-Algorithmus\n",
    "\n",
    "Der Floyd-Warshall-Algorithmus berechnet die k\u00fcrzeste Verbindungen zwischen allen Paaren von Vertizes in einem Graphen.\n",
    "\n",
    "Ein *Graph* ist eine Menge von *Vertizes* zusammen mit einer Menge von *Kanten*. Jede Kante verbindet zwei Vertizes, und zwischen zwei gegebenen Vertizes existiert maximal eine Kante.\n",
    "\n",
    "$w(i, j)$ ist L\u00e4nge der Kante von Vertex $i$ nach Vertex $j$. Der Einfachheit halber werden die Vertizes mit $0, 1, \\ldots, n-1$ numeriert.\n",
    "\n",
    "Der Algorithmus berechnet Schrittweise die Funktion $s(i, j, k)$ f\u00fcr alle $i$ und $j$. Dabei ist $s(i, j, k)$ der k\u00fcrzeste Weg von $i$ nach $j$, der nur Vertizes ${1, \\ldots, k}$ als Zwischen-Vertex verwendet. Der Algorithmus erh\u00f6ht iterativ die Anzahl der zugelassenen Zwischen-Vertizes, bis der ganze Graph abgedeckt ist:\n",
    "\n",
    "1. Wenn man keine anderen Vertizes benutzen darf, dann ist der direkte Weg der K\u00fcrzeste.\n",
    "\n",
    "    $$s(i, j, 0) = w(i, j)$$\n",
    "\n",
    "2. Wenn ein weiterer Vertex zugelassen wird, dann ist der k\u00fcrzeste Weg entweder\n",
    "    - der bisher gefundene Weg, oder\n",
    "    - der bisherige k\u00fcrzeste Weg vom Startpunkt zum neuen Punkt, gefolgt vom bisherigen k\u00fcrzesten Weg vom neuen Punkt zum Endpunkt.\n",
    "    \n",
    "    $$s(i, j, k+1) = \\min\\left( s(i, j, k), s(i, k+1, k) + s(k+1, j, k) \\right).$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Data.Array.Repa\n",
    "import Data.Array.Repa.Repr.Vector\n",
    "\n",
    "type Graph a = Array V DIM2 a\n",
    "\n",
    "shortestPaths :: (Num a, Ord a) => Graph a -> Graph a\n",
    "shortestPaths graph = step graph 0\n",
    "    where _:.n = extent graph\n",
    "          step g k \n",
    "              | k == n    = g\n",
    "              | otherwise = let g' = computeVectorS $ fromFunction (ix2 n n) $ \\(Z:.i:.j) ->\n",
    "                                         min (g!(Z:.i:.j)) (g!(Z:.i:.k) + g!(Z:.k:.j))\n",
    "                            in step g' (k+1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Graphen mit nicht-verbundenen Vertizes\n",
    "\n",
    "... k\u00f6nnen modelliert werden, indem f\u00fcr $w(i, j)$ der Wert $\\infty$ zugelassen wird."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data Weight a = Weight a | Infinity deriving (Eq, Ord, Show)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`deriving Ord` funktioniert so, dass Datenkonstruktoren links vom `|` als kleiner betrachtet werden:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Weight 42 < Infinity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir ben\u00f6tigen ausserdem eine `Num`-Instanz. Diese ist formal nur bedingt sinnvoll, praktisch aber recht einfach:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "-- Functor macht einige Definitionen unten einfacher\n",
    "instance Functor Weight where\n",
    "    fmap f (Weight i) = Weight $ f i\n",
    "    fmap f Infinity = Infinity\n",
    "\n",
    "instance Num a => Num (Weight a) where\n",
    "    Weight i + Weight j = Weight $ i+j\n",
    "    _        + _        = Infinity\n",
    "\n",
    "    Weight i * Weight j = Weight $ i*j\n",
    "    _        * _        = Infinity\n",
    "    \n",
    "    fromInteger = Weight . fromInteger\n",
    "\n",
    "    negate = fmap negate\n",
    "    abs = fmap abs\n",
    "    signum = fmap signum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`(*)`, `abs` und `signum` werden nicht ben\u00f6tigt, sollte aber trotzdem deklariert werden. Eine andere M\u00f6glichkeit w\u00e4re, sie als `undefined` zu deklarieren.\n",
    "\n",
    "`negate` ist mathematisch nicht ganz sauber, da\n",
    "```haskell\n",
    "negate Infinity == Infinity\n",
    "```\n",
    "reicht f\u00fcr unsere Zwecke aber aus.\n",
    "\n",
    "\n",
    "Der Algorithmus funktioniert auch f\u00fcr gerichtete Kanten und sogar f\u00fcr negative Kanten-Gewichte, solange es keine *negativen Zyklen* gibt.\n",
    "\n",
    "#### Beispiel (von Wikipedia)\n",
    "\n",
    "![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Floyd-Warshall_example.svg/1324px-Floyd-Warshall_example.svg.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "AVector ((Z :. 4) :. 4) [Weight 0,Weight (-1),Weight (-2),Weight 0,Weight 4,Weight 0,Weight 2,Weight 4,Weight 5,Weight 1,Weight 0,Weight 2,Weight 3,Weight (-1),Weight 1,Weight 0]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "shortestPaths $ fromListVector (ix2 4 4)\n",
    "    [        0, Infinity,       -2, Infinity\n",
    "    ,        4,        0,        3, Infinity\n",
    "    , Infinity, Infinity,        0,        2\n",
    "    , Infinity,       -1, Infinity,        0\n",
    "    ]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
