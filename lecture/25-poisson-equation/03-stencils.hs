{-# OPTIONS_GHC -Odph -rtsopts -threaded -fno-liberate-case -funfolding-use-threshold1000 -funfolding-keeness-factor1000 #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE QuasiQuotes #-}

-- Operations like `laplace` in 02-repa.hs are a frequent pattern: each element
-- of the resulting array is a linear combination of the surrounding elements of
-- the input array.
--
-- Repa has special means for constructing such operations in 2-d using
-- stencils. Stencils are constructed with
--
--   makeStencil2 :: Num a => Int -> Int -> (DIM2 -> Maybe a) -> Stencil DIM2 a
--
-- and applied to 2-d arrays with
--
--   mapStencil2 :: Source r a => Boundary a -> Stencil DIM2 a -> Array r DIM2 a -> Array PC5 DIM2 a
--
-- `Boundary a` is a type that handles the boundary conditions. It can be
--     - `BoundFixed x` returns a fixed value `x :: a` at the boundary,
--     - `BoundConst x` assumes pixels outside the array have the value `x :: a`,
--     - `BoundClamp` extends the boundary values to outside the boundary.
--
-- `PC5` is a special representation for stencil results; it is defined as (no
-- need to understand this)
--
--     type PC5 = P C (P (S D) (P (S D) (P (S D) (P (S D) X))))
--
-- `PC5` arrays can be converted to `D` arrays using the polymorphic function
--
--     delay :: (Shape sh, Source r e) => Array r sh e -> Array D sh e

import Data.Array.Repa as R
import Data.Array.Repa.Helpers (runCG, saveArrayAsPNG)
import Data.Array.Repa.Stencil
import Data.Array.Repa.Stencil.Dim2

poisson :: Int -> (Double -> Double -> Double) -> IO (Array U DIM2 Double)
poisson n f = runCG 1e-6 negLaplace =<< computeP rhs
    where
      -- coordinates
      h = 1 / fromIntegral n :: Double
      xs :: Array U DIM1 Double
      xs = computeS $ fromFunction (Z:.n) $ \(Z:.(!j)) -> (fromIntegral j + 1/2) * h
      -- right hand side
      gridshape = Z:.n:.n :: DIM2
      rhs = fromFunction gridshape $ \(Z:.(!jx):.(!jy)) ->
            f (unsafeLinearIndex xs jx) (unsafeLinearIndex xs jy)
      -- laplace stencil
      negLaplaceStencil = makeStencil2 3 3 computeElem :: Stencil DIM2 Double
          where computeElem (Z:. -1 :.  0) = Just $ -c
                computeElem (Z:.  1 :.  0) = Just $ -c
                computeElem (Z:.  0 :. -1) = Just $ -c
                computeElem (Z:.  0 :.  1) = Just $ -c
                computeElem (Z:.  0 :.  0) = Just $ 4*c
                computeElem _              = Nothing
                c = 1/h**2
      -- laplace operator
      negLaplace :: Array U DIM2 Double -> Array D DIM2 Double
      negLaplace = delay . mapStencil2 (BoundConst 0) negLaplaceStencil

-- There is special syntax for stencils. The laplaceStencil above can be
-- produced more easily by
--
--   laplaceStencil = [stencil2|  0 -1  0
--                               -1  4 -1
--                                0 -1  0 |]
--   laplace = R.map (/h**2) . mapStencil2 (BoundConst 0) laplaceStencil
--
-- This syntax requires the QuasiQuotes language extension. The stencil is
-- constructed at compile time and can therefore not depend on runtime values.
-- That's why we have to use R.map to do the division by h**2 here.

main :: IO ()
main = saveArrayAsPNG "03.png" (500, 500) =<< poisson 50 (\x y -> x*y**4)
