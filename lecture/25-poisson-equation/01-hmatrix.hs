{-# OPTIONS_GHC -Odph -rtsopts -threaded -fno-liberate-case -funfolding-use-threshold1000 -funfolding-keeness-factor1000 #-}
{-# LANGUAGE BangPatterns #-}

import Data.Array.Repa
import Data.Array.Repa.Helpers (saveArrayAsPNG)
import Data.Maybe (fromJust)
import Numeric.LinearAlgebra.Helpers
import Numeric.LinearAlgebra.Repa (linearSolve)

poisson :: Int -> (Double -> Double -> Double) -> Mat Double
poisson n f = computeS . reshape gridshape . fromJust . linearSolve negLaplace $ asColumn rhs
    where
      -- coordinates
      h = 1 / fromIntegral n :: Double
      xs :: Array U DIM1 Double
      xs = computeS $ fromFunction (Z:.n) $ \(Z:.(!j)) -> (fromIntegral j + 1/2) * h
      -- right hand side
      gridshape = Z:.n:.n :: DIM2
      rhs = vec [ f (unsafeLinearIndex xs jx) (unsafeLinearIndex xs jy)
                | i <- [0..n*n-1] , let Z:.(!jx):.(!jy) = fromIndex gridshape i]

      -- Both the right hand side and the solution *should be* 2-d arrays of
      -- size (n x n), but linear algebra routines require them to be vectors.
      -- So we need a way to convert from a 1-d vector index j (0 <= j <= n^2-1)
      -- to a 2-d coordinate pair (xj, yj) (0 <= xj, yj <= n-1). Instead of
      -- reinventing it, we use repa's own functions to convert between linear
      -- and multi-dimensional indices:
      --
      --   fromIndex gridshape :: Int -> DIM2
      --
      -- converts 1-d to 2-d. The resulting vector is converted back to a 2-d
      -- array using
      --
      --   reshape gridshape :: Array r DIM1 Double -> Array D DIM2 Double

      matshape = Z:.(n*n):.(n*n) :: DIM2
      negLaplace :: Mat Double
      negLaplace = computeS $ fromFunction matshape computeElem
          where computeElem :: DIM2 -> Double
                computeElem (Z:.(!j):.(!k))
                    | j == k                         = 4*c
                    | xj == xk && abs (yj - yk) == 1 = -c
                    | yj == yk && abs (xj - xk) == 1 = -c
                    | otherwise                      = 0
                    where
                      Z:.(!xj):.(!yj) = fromIndex gridshape j :: DIM2
                      Z:.(!xk):.(!yk) = fromIndex gridshape k :: DIM2
                      c = 1/h**2

main :: IO ()
main = saveArrayAsPNG "01.png" (500, 500) $ poisson 50 (\x y -> x*y**4)
