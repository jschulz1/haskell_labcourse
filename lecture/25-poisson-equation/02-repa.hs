{-# OPTIONS_GHC -Odph -rtsopts -threaded -fno-liberate-case -funfolding-use-threshold1000 -funfolding-keeness-factor1000 #-}
{-# LANGUAGE BangPatterns #-}

import Data.Array.Repa
import Data.Array.Repa.Helpers (runCG, saveArrayAsPNG)

poisson :: Int -> (Double -> Double -> Double) -> IO (Array U DIM2 Double)
poisson n f = runCG 1e-6 negLaplace =<< computeP rhs
    where
      -- coordinates
      h = 1 / fromIntegral n :: Double
      xs :: Array U DIM1 Double
      xs = computeS $ fromFunction (Z:.n) $ \(Z:.(!j)) -> (fromIntegral j + 1/2) * h
      -- right hand side
      gridshape = Z:.n:.n :: DIM2
      rhs = fromFunction gridshape $ \(Z:.(!jx):.(!jy)) ->
            f (unsafeLinearIndex xs jx) (unsafeLinearIndex xs jy)
      -- laplace operator
      negLaplace :: Array U DIM2 Double -> Array D DIM2 Double
      negLaplace arr = fromFunction ext computeElem
          where
            !ext = extent arr
            get !idx | inShape ext idx = unsafeIndex arr idx
                     | otherwise       = 0
            computeElem (Z:.(!jx):.(!jy)) = ( 4*get (Z:.jx:.jy)
                                            - get (Z:.(jx-1):.jy) - get (Z:.(jx+1):.jy)
                                            - get (Z:.jx:.(jy-1)) - get (Z:.jx:.(jy+1)) )
                                            / h**2

main :: IO ()
main = saveArrayAsPNG "02.png" (500, 500) =<< poisson 50 (\x y -> x*y**4)
