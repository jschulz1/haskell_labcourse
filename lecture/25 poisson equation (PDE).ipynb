{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Poisson equation in 2d\n",
    "\n",
    "\n",
    "## Poisson equation\n",
    "\n",
    "Find  $u \\in C^2(\\Omega)\\cap C(\\overline{\\Omega})$ with\n",
    "\n",
    "$$\n",
    "\\begin{cases} \n",
    "  -\\Delta u  = f & \\text{in } \\Omega \\\\\n",
    "  u = 0 & \\mbox{on } \\partial \\Omega\n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "for $\\Omega = (0,1)^2$ and $f \\in C(\\Omega)$. For $f = 0$, the equation is known as *Laplace equation*.\n",
    "\n",
    "## Laplace operator\n",
    "\n",
    "$$\n",
    "\\Delta u := \\sum_{i=1}^d \\frac{\\partial ^2 u}{\\partial x_i^2}\n",
    "$$\n",
    "\n",
    "### Discretization\n",
    "\n",
    "- Equidistant grid $\\Omega_N = I_N \\times I_N$, where\n",
    "    \n",
    "    $$I_N := \\left\\{ x^N_j \\colon j = 0, \\ldots, N-1 \\right\\} \\subset (0, 1), \\qquad x^N_j := \\left( j + \\frac{1}{2} \\right) h_N, \\qquad h_N := \\frac{1}{N}.$$\n",
    "\n",
    "- Approximation of $\\frac{\\partial^2 u}{\\partial x^2}$:\n",
    "\n",
    "    $$\\frac{u(x-h, y) - 2 u(x, y) + u(x+h, y)}{h^2} = \\frac{\\partial^2 u}{\\partial x^2} (x, y) + \\mathcal{O}(h^2),$$\n",
    "    \n",
    "    and similarly for $\\frac{\\partial^2 u}{\\partial y^2}$.\n",
    "\n",
    "- Approximation of Laplace operator:\n",
    "\n",
    "    $$\\frac{u(x, y-h) + u(x-h, y) - 4 u(x, y) + u(x, y+h) + u(x+h, y)}{h^2} = \\Delta u(x, y) + \\mathcal{O}(h^2)$$\n",
    "    \n",
    "- Define \n",
    "\n",
    "    $$u^N_{jk} := u(x^N_j, x^N_k), \\qquad f^N_{jk} := f(x^N_j, x^N_k),$$\n",
    "    \n",
    "    for $j, k = 0, 1, \\ldots, N-1$, and\n",
    "    \n",
    "    $$(\\Delta^N u^N)_{jk} := \\frac{u^N_{j,k-1} + u^N_{j-1,k} - 4 u^N_{jk} + u^N_{j,k+1} + u^N_{j+1,k}}{h_N^2}.$$\n",
    "    \n",
    "    Here, $u^N_{-1,k} = u^N_{N,k} = u^N_{j,-1} = u^N_{j,N} = 0$ (boundary condition).\n",
    "    \n",
    "- Solve\n",
    "\n",
    "    $$-\\Delta^N u^N = f^N.$$\n",
    "    \n",
    "## Code\n",
    "\n",
    "For performance reasons, the code is not written in the notebook. See files `01-hmatrix.hs`, `02-repa.hs` and `03-stencils.hs`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "negLaplace :: Array U DIM2 Double -> Array D DIM2 Double\n",
    "negLaplace arr = fromFunction ext computeElem\n",
    "    where\n",
    "        !ext = extent arr\n",
    "        get !idx | inShape ext idx = unsafeIndex arr idx\n",
    "                 | otherwise       = 0\n",
    "        computeElem (Z:.(!jx):.(!jy)) = ( 4*get (Z:.jx:.jy)\n",
    "                                         - get (Z:.(jx-1):.jy) - get (Z:.(jx+1):.jy)\n",
    "                                         - get (Z:.jx:.(jy-1)) - get (Z:.jx:.(jy+1)) )\n",
    "                                         / h**2\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stencils\n",
    "Operations like `laplace` in 02-repa.hs are a frequent pattern: each element\n",
    "of the resulting array is a linear combination of the surrounding elements of\n",
    "the input array.\n",
    "\n",
    "Repa has special means for constructing such operations in 2-d using stencils. Stencils are constructed with\n",
    "\n",
    "``` haskell\n",
    "makeStencil2 :: Num a => Int -> Int -> (DIM2 -> Maybe a) -> Stencil DIM2 a\n",
    "```\n",
    "\n",
    "and applied to 2-d arrays with\n",
    "\n",
    "``` haskell\n",
    "mapStencil2 :: Source r a => Boundary a -> Stencil DIM2 a -> Array r DIM2 a -> Array PC5 DIM2 a\n",
    "```\n",
    "\n",
    "`Boundary a` is a type that handles the boundary conditions. It can be     \n",
    "    - `BoundFixed x` returns a fixed value `x :: a` at the boundary,\n",
    "    - `BoundConst x` assumes pixels outside the array have the value `x :: a`,\n",
    "    - `BoundClamp` extends the boundary values to outside the boundary.\n",
    "\n",
    "`PC5` is a special representation for stencil results; it is defined as (no\n",
    "need to understand this)\n",
    "\n",
    "``` haskell\n",
    "type PC5 = P C (P (S D) (P (S D) (P (S D) (P (S D) X))))\n",
    "```\n",
    "\n",
    "`PC5` arrays can be converted to `D` arrays using the polymorphic function\n",
    "\n",
    "``` haskell\n",
    "delay :: (Shape sh, Source r e) => Array r sh e -> Array D sh e\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "negLaplaceStencil = makeStencil2 3 3 computeElem :: Stencil DIM2 Double\n",
    "    where \n",
    "        computeElem (Z:. -1 :.  0) = Just $ -c\n",
    "        computeElem (Z:.  1 :.  0) = Just $ -c\n",
    "        computeElem (Z:.  0 :. -1) = Just $ -c\n",
    "        computeElem (Z:.  0 :.  1) = Just $ -c\n",
    "        computeElem (Z:.  0 :.  0) = Just $ 4*c\n",
    "        computeElem _              = Nothing\n",
    "        c = 1/h**2\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is special syntax for stencils. The laplaceStencil above can be\n",
    "produced more easily by\n",
    "\n",
    "\n",
    "``` haskell\n",
    "    laplaceStencil = [stencil2|  0 -1  0\n",
    "                                -1  4 -1\n",
    "                                 0 -1  0 |]\n",
    "```\n",
    "```haskell\n",
    "   laplace = R.map (/h**2) . mapStencil2 (BoundConst 0) laplaceStencil\n",
    "```\n",
    "\n",
    "This syntax requires the QuasiQuotes language extension. The stencil is\n",
    "constructed at compile time and can therefore not depend on runtime values.\n",
    "That's why we have to use R.map to do the division by h**2 here.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
