% Übungsblatt: Endrekursion 


<!--
> modFibs :: Integral a => a -> a
> modFibs a = tailFibs 0 1 0 a
> 
> tailFibs:: Integral a => a -> a -> a -> a -> a
> tailFibs prev1 prev2 start end
>	 | start == end = next
>	 | otherwise = tailFibs next prev1 (start+1) end
>		where next = prev1 + prev2
> aufgabe3 = do
>	 putStrLn "\nAufgabe 3\n---------"
>	 print $ modFibs 30 
>	 print $ modFibs 1000

Das Hauptproblem besteht darin, dass die Standard Fibonacci-Implementation 
fuer stets 2 rekursive Aufruefe macht, deren Ergebnsis nicht feststeht. Das fuehrt dazu
dass sehr haeufig dieselbe fibonacci-Zahl berechnet wird und der Algorithmus exponentiell steigenden Aufwand hat.
-->
<!-- 
*Main> fibs 30
1346269
(7.67 secs, 1975748744 bytes)
*Main> modFibs 30
1346269
(0.01 secs, 1080192 bytes)
Unterschied in Geschwindigkeit UND v.a. Memorybenutzung



Aufgabe 5
---------

Schreibt eigene, endrekursive `even`/`odd` Funktionen, die euch nach Übergabe einer
ganzen Zahl zurückgeben, ob die Zahl gerade oder ungerade ist. Was
fällt auf? Sind die Funktionen für sich alleine genommen endrekursiv?

<!--
> modEven :: Integral a => a -> Bool
> modEven n 
>	| n == 0 = True
>	| otherwise = modOdd $ n-1
> 
> modOdd :: Integral a => a -> Bool
> modOdd n
>	| n ==0 = False
>	| otherwise = modEven $ n-1

> aufgabe5 = do
>	 putStrLn "\nAufgabe 5\n---------"
>	 print $ modOdd 42
>	 print $ modEven 42

Fuer sich alleine sind es keine endrekursive Funktionen, da sie eine andere Funktion aufrufen.
Der Vorteil der sich beendenden aufrufenden Funktion bleibt aber bestehen.
-->

