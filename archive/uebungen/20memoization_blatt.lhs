% Übungen: Memoization 

Aufgabe 1
---------

Benutzt `Map` um eine Funktion zu schreiben, die aus einem gegebenen String alle darin vorhandenen Chars mit ihrer vorhandenen Anzahl ausgibt.


Aufgabe 2
---------

Implementiere die Ackermann-Funktion mit `Memo`.

Berechne ack(4,1).
<!--

>import Control.Monad.Memo
>
>ackm (0,n) = return (n+1)
>ackm (m,0) = memo ackm ((m-1),1)
>ackm (m,n) = memo ackm (m,n-1) >>= \n1 -> memo ackm ((m-1),n1)
>
>ack = startEvalMemo . ackm

-->
