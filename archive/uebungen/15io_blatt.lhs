% Übungsblatt: IO
<!--
> import Data.CSV
> import Data.List.Split as S
> import Text.ParserCombinators.Parsec
> import System.IO
> import Data.Char
> import System.Random
-->




Aufgabe 6
---------

- Lasst einen Nutzer eine Liste von Zahlen eingeben, bildet in eigenen Funktionen
  Summe, Summe der Quadrate, Durchschnitt, Median und RMS, und gebt diese Werte aus.

- Nutzt in der Listen-Eingabe Maybe, für mehr Sicherheit durch Fehlerbehandlung.

<!--
TODO: loesung

> aufgabe6 = do
>   putStrLn "\nAufgabe 6\n---------"

-->

Aufgabe 7
---------

Erweitert Aufgabe 1 so, dass falsche Eingaben abgefangen und
erneut abgefragt werden.

<!--
TODO: loesung

> aufgabe7 = do
>   putStrLn "\nAufgabe 6\n---------"
-->

Aufgabe 8
---------

- Nutzt das CSV-Modul um `Person`en aus csv-Dateien einzulesen.

- Macht das Programm fehlersicher.

<!--
> checkErr :: Either ParseError [[String]] -> [[String]]
> checkErr a
>     | x == [["MEGAERROR PARSING"]] = error "MEGAERROR PARSING"
>     | otherwise = x
>     where x = either (\y -> [["MEGAERROR PARSING"]]) (\z -> z) a
>
> addPerson :: [String] -> Person
> addPerson attr = Person (head attr) (read (attr!!1)) (geschl) (attr!!3) (read (attr!!4):: Bool)
>     where geschl = if 'M' == (head attr!!2) then M else F
>
> makePersons :: [[String]] -> [Person] -> [Person]
> makePersons [] y = y
> makePersons (x:xs) y = makePersons xs ((addPerson x):y)

> aufgabe8 = do
>   --personData <- parseFromFile csvFile "filePath"
>   --let c = checkErr personData
>   --print c
>   --let d = makePersons c []
>   --print d
>   putStrLn "Vernueftige csv machen mit 1 Person und jew. Infos pro Zeile und ausfuehren."
-->

Aufgabe 9 
---------

Schreibt das Beispiel für Dateizugriff aus der Vorlesung so um, dass keine
`do`-Notation verwendet wird.

	import System.IO
	import Data.Char
	
	main :: IO ()
	main = do
	    putStrLn "Dateiname eingeben: " 
	    fn <- getLine
	    fhandle <- openFile fn ReadMode
	    content <- hGetLine fhandle
	    hClose fhandle
	    writeFile (fn ++ ".caps") (capitalize content)

	capitalize :: String -> String
	capitalize c = map toUpper c

<!--
> main' :: IO ()
> main' = 
>     putStrLn "Dateiname eingeben: "
>     >> getLine
>     >>= \fn -> (openFile fn ReadMode
>     >>= (\fh -> hGetLine fh
>     >>= \content -> hClose fh
>     >> writeFile (fn ++ ".caps") (capitalize content)))
>     where capitalize = map toUpper 

> aufgabe9 = do
>   putStrLn "\nAufgabe 9\n---------"
>   main'
-->

Aufgabe 10
---------

Implementiert ein Zahlen-Rate-Spiel, das nach folgendem Schema funktioniert:

    Denk an eine Zahl zwischen 1 und 100!
    Ist es 50?  _weniger_
    Ist es 25?  _mehr_
    ...
    Ist es 42?  _ja_ 
    Na toll.

<!--
> game :: String -> Int -> Int
> game "weniger" x = div x 2    -- hier bessere Logik moeglich
> game "mehr" x = x + div x 3
> game "ja" x = x 
> game _ x = -1

>
> oneGuess :: Int -> IO ()
> oneGuess x = do
>     putStrLn $ "Ist es " ++ show x ++ "? "
>     ans <- getLine
>     newx <- return . game ans
>     case newx of 
>          x  -> putStrLn "Na Toll."
>          -1 -> putStrLn "Antworte mit 'weniger', 'mehr' oder 'ja'!" >> oneGuess newx
>          _ -> oneGuess newx 
> 
> mainNumberguessing :: IO ()
> mainNumberguessing = do
>     putStrLn "Denk an eine Zahl zwischen 1 und 100!"
>     oneGuess 50

> aufgabe10 = do
>   putStrLn "\nAufgabe 10\n---------"
>   mainNumberguessing
>   -- TODO: benutze gamedata und mache funktionen die pur sind
-->

Aufgabe 11 
---------

CLI-Arguments: Schaut euch `getArgs` in `System.Environment` an.
Verwendet es, um die Optionen "-h" für Hilfe und "-v" für die Ausgabe der
Versionsnummer gesondert zu behandeln. Im Falle eines anderen Arguments soll die
Datei mit diesem Namen (sofern vorhanden) gelesen und zeilenweise rückwärts
ausgegeben werden; sonst die Standardeingabe.

<!--
import System.Environment
import System.Exit

main = getArgs >>= parse >>= putStr . tac

tac  = unlines . reverse . lines

parse ["-h"] = usage   >> exit
parse ["-v"] = version >> exit
parse []     = getContents
parse fs     = concat `fmap` mapM readFile fs

usage   = putStrLn "Usage: tac [-vh] [file ..]"
version = putStrLn "Haskell tac 0.1"
exit    = exitWith ExitSuccess
die     = exitWith (ExitFailure 1)

via http://www.haskell.org/haskellwiki/Tutorials/Programming_Haskell/Argument_handling

> aufgabe11 = do
>	  putStrLn "\nAufgabe 11\n---------"
-->

<!--
> callfun f x =  f >>  putStrLn $ "\nAufgabe " ++ x
> main = do
>   -- aufgabe1
>   -- let aufg = [aufgabe1]
>   -- return $ map (\x -> x ) aufg 
>   -- aufgabe5
>   aufgabe9 
-->
