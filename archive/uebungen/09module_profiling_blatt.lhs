

Aufgabe 3
---------

Schreibt ein eigenes Modul, welches eure Implementationen von der Berechnung der Fibonacci-Zahlen enthält und schreibt die Fibonacci-Folge so um, dass es dieses Modul benutzt.

<!--
Modul: 

module ModFibs (
modFibs) 
where

modFibs :: Integral a => a -> a
modFibs a = tailFibs 0 1 0 a

tailFibs:: Integral a => a -> a -> a -> a -> a
tailFibs prev1 prev2 start end
| start == end = next
| otherwise = tailFibs next prev1 (start+1) end
  where next = prev1 + prev2

und Folge:

import ModFibs
gkk :: Double -> Double -> Int -> (Int,Double)
gkk tol prevgk glied
| diff <= tol = ((glied-1),gk)
| otherwise = gkk tol gk (glied+1)
where
  diff = abs (prevgk - gk)
  gk = (fromIntegral $ modFibs glied) / (fromIntegral $ modFibs (glied-1))
-->


<!--
TODO:
    - module verwenden, was passiert bei namenskonflikt - lösen
    - compilieren, optionen
    - hlint?!
-->
