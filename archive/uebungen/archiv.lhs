
war in listen
Aufgabe 3
---------

<!-- Uebung fuer nen spaeteren Termin wenn auch Typen und pattern matching eingefuehrt wurden, aber finde das ganz gut und ist was theoretisches -->


TODO: hä?

<!-- - Genau wie die natürlichen Zahlen, sind auch Listen aufgrund ihrer Struktur für Beweise mit vollständiger Induktion prädestiniert.
Wo wir bei natürlichen Zahlen als Induktionsanfang (im Folgenden I.A.) n=0 (oder n=1) einsetzen, verwenden wir bei Listen [].
Im Induktionsschritt (I.S.)wird nicht von n auf n+1 geschlossen, sondern von xs auf x:xs.
Die einzelnen Umrechnungsschritte erfolgen durch pattern-matching auf unsere Definitionsgleichungen.

Definitionen:

(++)       :: [a]->[a]->[a]
[]++ys     =  ys
(x:xy)++ys =  x:(xs++ys)

rev        :: [a]->[a]
rev[]      =  []
rev(x:xs)  =  rev xs ++ [x]  

Satz:

rev (rev xs)=xs für alle endlichen Listen xs.

- Beweise durch vollständige Induktion : rev (rev xs ++[x]) = x:rev(rev xs)

I.A.: rev (rev []) = rev [] = []
I.S.: z.Z.: rev ( rev (x:xs)) = x:xs
Linke Seite:
      rev (rev (x:xs)) 
    =           {Definition von rev}
      rev (rev xs ++[x])
rechte Seite:
      x:xs
    =           {Induktionshypothese}
      x:rev (rev xs)
 z.Z.: rev (rev xs ++[x]) = x:rev(rev xs)
-->

Wenn man soweit ist: Verallgemeinerung durch vollstaendige Induktion zu :  rev (ys ++[x]) = x:rev ys

<!--
I.A.: rev([]++[x]) = rev([x]) = rev []++[x] = x:[] = x:rev []
I.S.: z.Z. rev ((y:ys) ++[x]) = x:rev (y:ys)
linke Seite:
      rev ((y:ys)++[x]
    =           {Definition von ++}
      rev (y:(ys++[x]))
    =           {Definition von rev}
      rev (ys++[x]) ++ [y]
    =           {Induktionsannahme}
      (x:rev ys) ++ [y]
    =           {Definition von ++}
      x: (rev ys ++[y])
    =           {Definition von rev}
      x: rev (y:ys)
[=rechte Seite]
-->



Aufgabe 7 
---------

Es seien die Funktionen `lesser`, `equal` und `greater` ... (wie auf dem
Rekursions-Blatt):

$\text{lesser}(v,l) = \{x : x \in l, x < v\}$
$\text{equal}(v,l) = \{x : x \in l, x = v\}$
$\text{greater}(v,l) = \{x : x \in l, x > v\}$

- der Code der drei (rekursiven) Funktionen unterscheidet sich nur an
  einer Stelle. 
  Implementiert nun _eine_ rekursive higher-order Funktion, die diese Tatsache
  ausnutzt. Nutzt sie für eine alternative Darstellung der drei Funktionen.
- Welche Listenfunktion kommt diese am ehesten und wie unterscheidet sie sich
  von ihr in der Implementierung (falls, _Hoogle_ verwenden)?

Test: 5 [1..10]

<!--
> compList _ _ [] = []
> compList f c (x:xs) 
>	| f x c			= x : compList f c xs 
>	| otherwise		= compList f c xs 
> lesser'	= compList (<)
> equal'	= compList (==)
> greater'	= compList (>)

> aufgabe7 = do
>	putStrLn "\nAufgabe 7\n---------"
>	print $ lesser' 5 [1..10]
>	print $ equal' 5 [1..10]
-->

Die Tschebyscheff-Polynome sind rekursiv durch die folgenden Formeln definiert:
T0(x)=1,T1(x)=x,Tk(x)=2xTk−1(x)−Tk−2(x).

Berechnen Sie die Liste bestehend aus T0(x),…,T9(x), in dem Sie sukzessive Elemente anhängen. Bestimmen Sie jeweils den Funktionswert an x=1/3 und x=0.33 und bestimmen Sie die Nullstellen!
