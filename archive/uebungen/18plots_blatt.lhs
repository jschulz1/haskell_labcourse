% Übungen: Plots

<!--
>import Graphics.Rendering.Chart 
>import Graphics.Rendering.Chart.Gtk
>import Graphics.Rendering.Chart.Backend.Cairo
>import Data.Default.Class
>import Control.Lens
>import Data.Colour
>import Data.Colour.Names
>import Numeric.Container
>import Data.Maybe
>main = do
>  aufgabe5
-->


Aufgabe 1
---------

Seien $y_1,y_2$ zwei Punkte im $\mathbb{R}^2$. Wir betrachten die Strecke mit
Endpunkten $y_1$ und $y_2$. Wir ersetzen  diese Strecke durch 4 Strecken 
$\overline{y_1 z_1}$, $\overline{z_1 z_2}$, $\overline{z_2 z_3}$,
$\overline{z_3 y_2}$ mit Endpunkten $z_1=\frac23 y_1 + \frac13 y_2$,
$z_3=\frac13 y_1 + \frac23 y_2$ und 

$$z_2 = \frac{\sqrt{3}}{6} \left( \begin{array}{cc}
0 & 1 \\ -1 & 0 \\
\end{array} \right)
(y_1 - y_2) + \frac12 (y_1 + y_2).$$

Analog zum Beispiel des Sierpinski-Dreiecks soll jede neue Teilstrecke
wiederum mittels der gleichen Prozedur durch 4 Strecken ersetzt werden. 
Schreibt ein Programm, dass
diese Prozedur $k$-mal wiederholt und das Ergebnis grafisch ausgibt.

Hinweis: Hier ist `Diagrams` geeigneter als `Chart`

<!--

Strecken
hruleEx = vcat' (with & sep .~ 0.2) (map hrule [1..5])
        # centerXY # pad 1.1
angles
-->

Aufgabe 2
--------

Erstellt eine Funktion, die zu einer gegebenen natürlichen
Zahl $n$ ein regelmäßiges $n$-Eck zeichnet.      

Wendet auf die Kanten eines regelmäßigen Sechsecks die rekursive
Funktion aus Aufgabe 1 an.   

*Hinweis*: Die Eckpunkte $(x_i,y_i)$ sind 

$$x_i=\sin( 2 {\pi i}/{n} ), \quad  y_i=\cos( 2 {\pi i}/{n} ),
  \quad i=1, \dots ,n$$


<!--
ebenfalls diagrams basic polygons aber wahrscheinlich einfacher mit lines 
-->

Aufgabe 3
---------

Schreibt ein Programm, dass zu einem gegebenen $a>0$ die
  Funktion
$$f(x):= 1/(x^2+a)$$
auf dem Intervall $[-3,3]$ plottet. 

Wie kann man euer Programm verallgemeinern, indem die übergebene Funktion beliebig sein kann aber auch noch Parameter enthalten können soll?


<!--
linspace
f = 1/(x^2+a)
-->

Aufgabe 4
---------

- Betrachtet die Datei `daten.dat`.
- Schreibt ein Programm, dass die Daten importiert und die
  Funktion anhand der gegebenen Daten geeignet plottet.

<!--
Data.Csv
-->

Aufgabe 5
---------

Versucht die folgende Grafik zu erstellen:

![Sinusse](../images/sinusse.pdf)

<!--
>chart = toRenderable layout
>  where
>    dt = toList $ linspace 200 (0,1::Double)
>    sin1 = plot_lines_values .~ [[ (x,sin (x*3.14159)) | x <- dt]]
>              $ plot_lines_style  . line_color .~ opaque blue
>              $ plot_lines_title .~ "sin (pi x)"
>              $ def
>
>    sin2 = plot_lines_values .~ [[ (x,sin (x*2*3.14159)) | x <- dt]]
>              $ plot_lines_style  . line_color .~ opaque green
>              $ plot_lines_style  . line_dashes .~ [4,4]
>              $ plot_lines_title .~ "sin (2*pi x)"
>              $ def
>
>    layout = layout_title .~ "Sinusse"
>           $ layout_x_axis . laxis_title .~ "x"
>           $ layout_y_axis . laxis_title .~ "y"
>           $ layout_x_axis . laxis_title_style . font_size .~ 20
>           $ layout_y_axis . laxis_title_style . font_size .~ 20
>           $ layout_legend .~ Just (legend_label_style . font_size .~ 20 $ def) 
>           $ layout_plots .~ [toPlot sin1,
>                              toPlot sin2]
>           $ def
>
>aufgabe5 = do
>    renderableToWindow chart 800 600 
>    renderableToFile (fo_size .~ (800,600) $ fo_format .~ PDF $ def) chart "sinusse.pdf"
-->
