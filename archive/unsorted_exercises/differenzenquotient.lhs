% Differenzenquotient und Rundungsfehler

**Vorraussetzung**: 1-12, 18

Differenziere $f(x)=exp(x)$ in $x=0$ durch den zentralen Differenzen-
quotienten. Plotte den Approximationsfehler für die Approximation der
ersten Ableitung durch den zentralen Differenzenquotienten für die 
Exponentialfunktion an der Stelle $x=0$ mit doppelt logarithmischen Achsen und
interpetieren sie das Ergebnis.

Tips:
\begin{enumerate}
\item Baut einen Vektor, der eine passende Anzahl von positiven
h–Werten $h_1 \ldots h_n$ enthält.
\item Baut euch daraus Vektoren, die die Werte $exp(h_j)$ bzw. $exp(-h_j)$
enthalten, und dann
\item einen Vektor, der alle zentralen Differenzenquotienten enthält.
\item Berechnet dann den Vektor, der die absoluten Fehler enthält,
\item und plottet ihn gegen den Vektor der $h$-Werte.
\item Macht einen doppelt logarithmischen
Plot macht.
\item Vermutlich werdet Ihr Gründe haben, eure Wahl der $h_j$ noch einmal
zu revidieren, um den Effekt klarer herauskommen zu lassen.
\end{enumerate}

<!--

>import Graphics.EasyPlot
>--import Graphics.Rendering.Chart 
>
>asdf ::  [Double] -> [Double] -> [Double] -> [Double] -> [Double]
>asdf x y z d 
>    | x==[] || y ==[] || z==[] = d
>    | x/=[] || y/=[] || z/=[] = asdf as bs cs (((a-b)/(2*c)):d)
>    | otherwise = error "ASDASDSD"
>    where (a:as)=x
>          (b:bs)=y
>          (c:cs)=z
>
>qwer :: [Double] -> [Double] -> [(Double,Double)] -> [(Double,Double)]
>qwer x y r
>    | x==[] || y == [] = r
>    | otherwise = qwer xs ys ((a,b):r)
>    where (a:xs)=x
>          (b:ys)=y
>
>main = do
>    let naive = [0.01,0.02..1]
>    --print naive
>    let sophisticated = [x^5 | x <- naive]
>    --print sophisticated
>    let enaiveP= [exp x | x<-naive]
>    --print enaiveP
>    let enaiveN= [exp(-x)| x<-naive]
>    --print enaiveN
>    let esophP = [exp(x) | x<-sophisticated]
>    --print esophP
>    let esophN = [exp (-x)| x<-sophisticated]
>    --let naiveCentral = [(x-y)/(2*z)| x<-enaiveP, y<-enaiveN, z<-naive]
>    let naiveCentral = asdf (reverse enaiveP) (reverse enaiveN) (reverse naive) []
>    --print naiveCentral
>    --let sophCentral = [(x-y)/(2*z) | x<-esophP, y<-esophN,z<-sophisticated]
>    let sophCentral = asdf (reverse esophP) (reverse esophN) (reverse sophisticated) []
>    let absNaive = [abs (x-1.0) | x<-naiveCentral]
>    let absSoph = [abs (x-1.0) | x<-sophCentral]
>    --let naivePlot = [(x,y) | x<-absNaive,y<-naive]
>    let naivePlot = qwer naive absNaive []
>    let sophPlot = qwer sophisticated absSoph []
>    --print naivePlot
>    --plot X11 $ Data2D [Title "Errors Nullstelle"] naivePlot
>    let loglogNaive = [log x | x<-sophisticated]
>    print loglogNaive
>    let loglogBlah = [log z | z<-absSoph]
>    let loglogPlot = qwer loglogNaive loglogBlah []
>    plot X11 $ Data2D [Style Lines,Title "Sample Data"] [] loglogPlot 
>    plot X11 $ Data2D [Style Lines,Title "Sample Data"] [] naivePlot 

-->
