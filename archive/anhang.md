# Anhang

## Ressourcen

### Tutorials

- [Tutorials](http://www.haskell.org/haskellwiki/Tutorials)

- [Try Haskell](http://tryhaskell.org/) 

- [SO: getting startet with haskell](http://stackoverflow.com/questions/1012573/getting-started-with-haskell)

- [$\lambda$ lessons](https://stevekrouse.github.io/hs.js/)


### Übungsaufgaben

- [Ninety-Nine Haskell Problems](http://www.haskell.org/haskellwiki/H-99:_Ninety-Nine_Haskell_Problems)
- [Project Euler](https://projecteuler.net/)
- [CS 154 Lab: Haskell Exercises](http://www.willamette.edu/~fruehr/154/labs/)
- [LYAHFGG exercises](https://github.com/noelmarkham/learn-you-a-haskell-exercises)
- [20 intermediate exercises](https://www.fpcomplete.com/user/DanBurton/20-intermediate-exercises)


### Videos

- [Chess Board](http://youtu.be/ScS8Q32lMxA)


## Literatur 
 \begin{thebibliography}{10}
      \small
    \bibitem{1} \alert{Learn You a Haskell for Great Good!}, M. Lipovača (\url{http://learnyouahaskell.com/}),
    \bibitem{2} \alert{Real World Haskell}, B. O'Sullivan, D. Stewart, J. Goerzen (\url{http://book.realworldhaskell.org}),
    \bibitem{3} \alert{Prägnante Programmierung in Haskell}, R. Grimm (\url{http://www.linux-magazin.de/Ausgaben/2011/06/Haskell}),
    \bibitem{4} \alert{Haskellreport}, diverse (\url{http://www.haskell.org/onlinereport/}),
  \end{thebibliography}



