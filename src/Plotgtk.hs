module Plotgtk
    ( renderFigure
    ,  plotX11
    ) where  

import Diagrams.Prelude
import Diagrams.Backend.Cairo
import Diagrams.Backend.Gtk
import Control.Monad.Trans (liftIO)
import Graphics.UI.Gtk 

renderFigure :: Diagram Cairo R2 -> EventM EExpose Bool 
renderFigure dia = do
    win <- eventWindow
    sizex <- liftIO $ drawWindowGetWidth win
    sizey <- liftIO $ drawWindowGetHeight win
    liftIO $ renderToGtk win $ toGtkCoords (sized (Dims (fromIntegral sizex) (fromIntegral sizey)) (pad 1.1 $dia))
    return True

plotX11 ::  Diagram Cairo R2 -> Int -> Int -> IO ()
plotX11 dia width heigth= do
  initGUI
  window <- windowNew
  canvas <- drawingAreaNew
  canvas `on` sizeRequest $ return (Requisition width heigth)
  set window [ containerBorderWidth := 10,
    containerChild := canvas ]
  canvas `on` exposeEvent $ renderFigure dia 
  onDestroy window mainQuit
  widgetShowAll window
  mainGUI

