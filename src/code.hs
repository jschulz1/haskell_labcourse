import Data.List as A

f x y = (x+y)/3

g a b c = if max a b == a && max a c == a
    then a
    else if max a b == b && max b c == b
    then b
    else c 

add i j = if j==0 
    then i
    else succ $ add i (j-1)

oddFifty list = [if odd x then True else False | x <- list]

containsZero :: (Num a, Show a) => a -> (String,String) -> Bool
containsZero x y = if '0' `elem` show x then
    (read $ fst y ) :: Bool
    else (read $ snd y ) :: Bool

jumpingFunction :: (Num a, Show a) => (a->(String,String)-> Bool) -> a -> (String, String) -> a
jumpingFunction g x y = if g x y == True then x-x*3  
    else x+x*3

jumpingFunctionMod :: (Num a, Show a) => (a->(String,String)-> Bool) -> a -> (String, String) -> a
jumpingFunctionMod g x y = if g x y == True then jumpingFunctionMod g (x-x*3) y 
    else jumpingFunctionMod g (x+x*3) y

mean :: Fractional a => [a] -> a
mean l = (sum l)/(Main.genericLength l)

--mean :: Integral a => [a] -> a
--mean l = (sum l)/(length l)

mean1 :: (Real a,Fractional b) => [a] -> b
mean1 l = realToFrac (sum l)/ realToFrac(length l)

genericLength           :: (Num i) => [a] -> i
genericLength []        =  0
genericLength (_:l)     =  1 + Main.genericLength l
--meanGen :: Num a => [a] -> a
--meanGen l = (sum l)/(length l)
--
--
--Listenlaenge erstellen mit foldl funct 0 [a]
funct :: (Real b) => b -> a -> b
funct i j = i+1

--Mittelwert foldl
--
meanFold :: (Real a, Fractional b) => [a] -> b
meanFold i = realToFrac((foldl (+) 0 i)) / realToFrac((foldl funct 0 i))

--median :: (Real a, Ord a,Fractional a,Real b,Fractional b) => [a] -> b
--median x = if c `mod` 2 == 0 then
   --(realToFrac(x !! (c)) + realToFrac(x !! (c+1)))/2
    --else realToFrac(x !! (c +0.5))
    --where b = A.sort x
          --c = realToFrac(length x)/realToFrac(2.0)

rectangleAreaInt :: Int -> Int -> Int
rectangleAreaInt a b = a*b
rectangleArea :: (Num a) => a -> a -> a
rectangleArea a b = a*b

tupel :: (String,String,String) -> String
tupel (a,b,c) = if a > b && a > c then a
    else if b > a && b > c then b
    else c



head1 :: [a] -> a
head1 [] = error "Can't call head on empty list"
head1 (x:xs) = x

reverseMe :: String -> String
reverseMe [] = []
reverseMe (x:xs) = reverseMe xs ++ [x]

calcArea :: (RealFloat a) => [(a, a)] -> [a]
calcArea xs = [area l w | (l, w) <- xs]
    where area length width = length* width 

checkIntArea :: Integer -> String
checkIntArea int
    | int >= -2147483648 && int <= 2147483647 = show int
    | otherwise = "NaN"


modEven :: Integral a => a -> Bool
modEven n 
        | n == 0 = True
        | otherwise = modOdd $ n-1

modOdd :: Integral a => a -> Bool
modOdd n 
       | n ==0 = False
       | otherwise = modEven $ n-1



modMod :: Integral a => a -> a -> a
modMod x y 
    | x < y = x
    | otherwise = modMod (x-y) y

multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z


elem' :: (Eq a) => a -> [a] -> Bool
elem' y ys = foldl (\acc x -> if x == y then True else acc) False ys

elem'' :: (Eq a) => a -> [a] -> Bool
elem'' y ys = foldr (\x acc -> if x == y then True else acc) False ys

revMe :: [a] -> [a]
revMe a = foldr (\x acc -> acc ++ [x]) [] a


