longestStr :: [String] -> String
longestStr list = foldr (\x acc -> if length x > length acc then x else acc) "" list
